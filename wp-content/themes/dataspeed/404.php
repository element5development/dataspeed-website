<?php /*
THE TEMPLATE FOR DISPLAYING 404 PAGES (BROKEN LINKS)
*/ ?>

<?php get_header(); ?>

<main class="full-width">
	<section class="not-found">

		<section class="white-papers-container max-width">
      <div class="white-papers clearfix">
        <h1>It seems this page has been lost.</h1>
        <p>Don't worry our robots have started to search through our archives to find it. In the mean time you can try performing your own search.</p>
        <?php get_search_form(); ?>
      </div>
    </section>

	</section>
</main>

<?php get_footer(); ?>
