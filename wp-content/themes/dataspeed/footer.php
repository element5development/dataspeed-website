<?php /*
THE FOOTER TEMPLATE FOR OUR THEME
*/ ?>
      
<!-- SITE FOOTER -->
  <?php get_template_part( 'template-parts/content', 'page-footer' ); ?>
<!-- OPEN AND CLOSE MENU -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/navanimation.js"></script>
<!-- SMOOTH SCROLLING -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/smoothscroll.js"></script>
<!-- STICKY -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.sticky-kit.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/stickyparts.js"></script>
<!-- ENTRANCE ANIMATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/entranceanimation.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.viewportchecker.min.js"></script>
<!-- SLICK SLIDERS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/slick.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/slickdeclaration.js"></script>
<!-- FLIPSTER -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flipster.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/flipsterdeclaration.js"></script>
<!-- FANCY BOX -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.fancybox-media.js"></script>
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/fancydeclaration.js"></script>
<!-- FILE UPLOAD -->
  <script>
    jQuery(function($) {
      $('input[type="file"]').change(function() {
        if ($(this).val()) {
          error = false;
          var filename = $(this).val();
          var filnameShort = filename.substring(12, 32) + "..."; 
          $(this).closest('#field_3_6').find('#extensions_message').html(filnameShort);
          if (error) {
            parent.addClass('error').prepend.after('<div class="alert alert-error">' + error + '</div>');
          }
        }
      });
    });
  </script>
  
</body>

<?php wp_footer(); ?>

</html>