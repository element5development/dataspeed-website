<?php

//SETUP WORDPRESS & BACKEND
  // REMOVE PARENT WIDGET AREAS
    function remove_parent_theme_widgets(){
      unregister_sidebar( 'sidebar-1' );
      unregister_sidebar( 'sidebar-2' );
      unregister_sidebar( 'sidebar-3' );
    }
    add_action( 'widgets_init', 'remove_parent_theme_widgets', 11 );
  // REMOVE PARENT THEME MENUS
    function remove_default_menu(){
      unregister_nav_menu('primary');
      unregister_nav_menu('social');
    }
    add_action( 'after_setup_theme', 'remove_default_menu', 11 );
  // ENABLE JQUERY TO WORDPRESS
      if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
      function my_jquery_enqueue() {
         wp_deregister_script('jquery');
         wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js", false, null);
         wp_enqueue_script('jquery');
      }
  // ENABLE SHORTCODES IN WIDGETS
    add_filter('widget_text', 'do_shortcode');
  // ENABLE ACF OPTIONS PAGE
    if( function_exists('acf_add_options_page') ) {
      acf_add_options_page();
      acf_add_options_sub_page('Theme-Options');
      acf_add_options_sub_page('Styleguide');
    }
  // ALLOW SVG & ZIP FILES IN WORDPRESS
    function cc_mime_types($mimes) {
      $mimes['svg'] = 'image/svg+xml';
      $mimes['zip'] = 'application/zip';
      return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');
  // ALLOW EDITOR ACCESS TO GRAVITY FORMS, MENUS & WIDGETS
    function add_grav_forms(){
      $role = get_role('editor');
      $role->add_cap('gform_full_access');
      $role->add_cap('edit_theme_options');
      $role->add_cap('edit_users');
      $role->add_cap('list_users');
      $role->add_cap('promote_users');
      $role->add_cap('create_users');
      $role->add_cap('add_users');
      $role->add_cap('delete_users');
    }
    add_action('admin_init','add_grav_forms');
  // ADD SMOOTHSCROLL CLASS TO MENU LINKS WITH REL
      function add_menuclass($ulclass) {
        return preg_replace('/<a rel="smoothScroll"/', '<a rel="smoothScroll" class="smoothScroll"', $ulclass);
      }
      add_filter('wp_nav_menu','add_menuclass');
  //LIMIT EXCERPT LENGTH
      function get_excerpt(){
      $excerpt = get_the_content();
      $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
      $excerpt = strip_shortcodes($excerpt);
      $excerpt = strip_tags($excerpt);
      $excerpt = substr($excerpt, 0, 300);
      $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
      $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
      $excerpt = $excerpt.'...';
      return $excerpt;
      }
  // STYLEGUIDE HEX TO RGB
    function hex2rgb( $colour ) {
      if ( $colour[0] == '#' ) { $colour = substr( $colour, 1 ); }
      if ( strlen( $colour ) == 6 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
      } elseif ( strlen( $colour ) == 3 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
      } else {
        return false;
      }
      $r = hexdec( $r ); $g = hexdec( $g ); $b = hexdec( $b );
      return 'rgb('.$r.', '.$g.', '.$b.')';
    }
  // STYLEGUIDE RGB TO CMYK
    function hex2rgb2($hex) {
      $color = str_replace('#','',$hex);
      $rgb = array(
        'r' => hexdec(substr($color,0,2)),
        'g' => hexdec(substr($color,2,2)),
        'b' => hexdec(substr($color,4,2)),
      );
      return $rgb;
    }
    function rgb2cmyk($var1,$g=0,$b=0) {
      if (is_array($var1)) { $r = $var1['r']; $g = $var1['g']; $b = $var1['b'];
      } else { $r = $var1; }
      $r = $r / 255; $g = $g / 255; $b = $b / 255;
      $bl = 1 - max(array($r,$g,$b));
      $c = ( 1 - $r - $bl ) / ( 1 - $bl ); $m = ( 1 - $g - $bl ) / ( 1 - $bl ); $y = ( 1 - $b - $bl ) / ( 1 - $bl );
      $c = round($c * 100); $m = round($m * 100); $y = round($y * 100); $bl = round($bl * 100);
      return 'cmyk('.$c.', '.$m.', '.$y.', '.$bl.')';
    }
//SETUP WORDPRESS & BACKEND


//CUSTOMIZE CONTENT FOR CLIENT
  // ADD MENUS FOR NAVIGATION
    function register_menu () {
      register_nav_menu('main-menu', __('Main Menu'));
      register_nav_menu('post-category-nav', __('Post Categories'));
      register_nav_menu('resource-category-nav', __('Resource Categories'));
    }
    add_action('init', 'register_menu');
  // PRIMARY BUTTON SHORTCODE
    function primary_button($atts, $content = null) {
      extract( shortcode_atts( array(
        'target' => '',
        'url' => '#',
        'arrow' => ''
      ), $atts ) );
      return '<a target="'.$target.'" href="'.$url.'" class="primary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
    }
    add_shortcode('primary-btn', 'primary_button');
  // SECONDARY BUTTON SHORTCODE
    function secondary_button($atts, $content = null) {
      extract( shortcode_atts( array(
        'target' => '',
        'url' => '#',
        'arrow' => ''
      ), $atts ) );
      return '<a target="'.$target.'" href="'.$url.'" class="secondary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
    }
    add_shortcode('secondary-btn', 'secondary_button');
  // LEAGAL TEXT SHORTCODE
    function legal_text($atts, $content = null) {
      extract( shortcode_atts( array( ), $atts ) );
      return '<p class="legal-text">' . do_shortcode($content) . '</p>';
    }
    add_shortcode('legal-text', 'legal_text');
  //ADD CUSTOM POST TYPE
    function create_posttype() {
      // Case Studies
      $labels = array(
          'name'                => _x( 'Case Studies', 'Post Type General Name', 'twentysixteen' ),
          'singular_name'       => _x( 'Case Study', 'Post Type Singular Name', 'twentysixteen' ),
      );
      $args = array(
          'label'               => __( 'Case Studies', 'twentysixteen' ),
          'labels'              => $labels,
          'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
          'taxonomies'          => array( 'categories' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => true,
          'capability_type'     => 'page',
      );
      register_post_type( 'case-studies', $args );
      // Resource
      $labels = array(
          'name'                => _x( 'Resources', 'Post Type General Name', 'twentysixteen' ),
          'singular_name'       => _x( 'Resource', 'Post Type Singular Name', 'twentysixteen' ),
      );
      $args = array(
          'label'               => __( 'Resources', 'twentysixteen' ),
          'labels'              => $labels,
          'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
          'taxonomies'          => array( 'category' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => true,
          'capability_type'     => 'page',
      );
      register_post_type( 'Resources', $args );
      // Customers
      $labels = array(
          'name'                => _x( 'Clients', 'Post Type General Name', 'twentysixteen' ),
          'singular_name'       => _x( 'Client', 'Post Type Singular Name', 'twentysixteen' ),
      );
      $args = array(
          'label'               => __( 'Clients', 'twentysixteen' ),
          'labels'              => $labels,
          'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
          'taxonomies'          => array( 'categories' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'can_export'          => true,
          'has_archive'         => false,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => true,
          'capability_type'     => 'page',
      );
      register_post_type( 'Clients', $args );
      // Careers
      $labels = array(
          'name'                => _x( 'Career', 'Post Type General Name', 'twentysixteen' ),
          'singular_name'       => _x( 'Career', 'Post Type Singular Name', 'twentysixteen' ),
      );
      $args = array(
          'label'               => __( 'Career', 'twentysixteen' ),
          'labels'              => $labels,
          'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
          'taxonomies'          => array( 'categories' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => true,
          'publicly_queryable'  => true,
          'exclude_from_search' => true,
          'capability_type'     => 'page',
      );
      register_post_type( 'Career', $args );
    }
    add_action( 'init', 'create_posttype' );

//CUSTOMIZE CONTENT FOR CLIENT


//REDIRECT WOOCOMMERCE IF NOT LOGGED IN (Customer Portal)

function customer_portal_redirect() {
    if (
        ! is_user_logged_in()
        && (is_woocommerce() || is_cart() || is_checkout())
    ) {
        // feel free to customize the following line to suit your needs
        wp_redirect(site_url('my-account/'));
        exit;
    }
}
add_action('template_redirect', 'customer_portal_redirect');

//REDIRECT WOOCOMMERCE IF NOT LOGGED IN (Customer Portal)

//REMOVE BREADCRUMBS
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
//REMOVE BREADCRUMBS

//REMOVE PRODUCT DESCRIPTION TITLE ON SINGLE PRODUCTS
add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
return '';
}
//REMOVE PRODUCT DESCRIPTION TITLE ON SINGLE PRODUCTS

// Remove WooCommerce Updater
remove_action('admin_notices', 'woothemes_updater_notice');
// Remove WooCommerce Updater
?>