<?php /*
THE TEMPLATE FOR DISPLAYING CATEGORY FOR BLOG
*/ ?>

<?php get_header(); ?>

<main class="full-width">

  <!-- PAGE TITLE, FEATURED IMAGE, BREADCRUMBS -->
    <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- GET CATEGORY -->
    <?php 
      $cat = get_query_var('cat');
      $category = get_category ($cat);
      $categoryName = $category->slug;
    ?>

  <!-- CATEGORY MENU -->
    <nav class="secondary-nav max-width">
      <h2>Sort by Category</h2>
      <?php if ( $categoryName == 'code-repository' || $categoryName == 'product-spec-sheets' ||  $categoryName == 'videos' ) { ?>
        <?php wp_nav_menu( array( 'theme_location' => 'resource-category-nav' ) ); ?>
      <?php } else { ?>
        <?php wp_nav_menu( array( 'theme_location' => 'post-category-nav' ) ); ?>
      <?php } ?>
    </nav>

  <!-- POST ARCHIEVE -->
    <section class="blog category-filter max-width clearfix">
      <?php echo do_shortcode('[ajax_load_more post_type="post, resources" " posts_per_page="6" scroll="false" transition_container="false" category="'.$categoryName.'" button_label="Load More"]'); ?>
    </section>

  <!-- SUPPORTERS LOGO SLIDER -->
    <?php get_template_part( 'template-parts/content', 'logo-slider' ); ?>

</main>

<?php get_footer(); ?>