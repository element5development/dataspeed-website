<?php /*
Template Name: Landing
*/ ?>

<?php get_header(); ?>

<main class="full-width">

    <?php get_template_part( 'template-parts/content', 'page-top' ); //PAGE TITLE, FEATURED IMAGE, BREADCRUMBS ?>

    <?php get_template_part( 'template-parts/content', 'landing' ); //PAGE TITLE, FEATURED IMAGE, BREADCRUMBS ?>

    <?php get_template_part( 'template-parts/content', 'clients' ); //CLIENT POST FEED ?>

    <?php get_template_part( 'template-parts/content', 'contact-preview' ); //CONTACT FORM, CONTACT INFRO AND GOOGLE MAP ?>

    <?php get_template_part( 'template-parts/content', 'white-papers' ); //WHITE PAPER DOWNLOADS ?>

</main>

<?php get_footer(); ?>