<?php /*
SORTABLE TABLE OF JOB OPENINGS
*/ ?>

<section class="openings full-width">
  <div class="max-width">
    <h2>No Current Openings</h2>
    <p>We currently have no job opportunities available. Please check back at a later time.</p>
  </div>
</section>