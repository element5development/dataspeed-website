<?php /*
DISPLAY CONTACT FORM, CONTACT INFRO AND GOOGLE MAP
*/ ?>

  <section class="contact-preview full-width clearfix">
    <div class="max-width">
      <p class="contact-header">Contact Us</p>
      <div class="one-half"><?php echo do_shortcode('[gravityform id="4" title="false" description="false"]'); ?></div>
      <div class="one-half">
        <ul>
          <li><a target="_blank" href="https://www.google.com/maps/place/1935+Enterprise+Dr,+Rochester+Hills,+MI+48309/@42.6414991,-83.171129,17z/data=!3m1!4b1!4m5!3m4!1s0x8824c1f4da48dbb5:0x8478cf7668b2cdd4!8m2!3d42.6414991!4d-83.1689403" class="address">1935 Enterprise Drive, Rochester Hills, MI 48309</a></li>
          <li><a href="tel:+1-<?php the_field('phone', 'options') ?>" class="phone">+1 <?php the_field('phone', 'options') ?></a></li>
          <li><a class="fax">+1 <?php the_field('fax', 'options') ?></a></li>
          <li><a href="mailto:info@dataspeedinc.com" class="email">info@dataspeedinc.com</a></li>
        </ul>
      </div>
    </div>
    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCPFqSdsyB_vfpUfPZaGglFEeSyRgZV6z0'></script><div style='overflow:hidden;height:400px;width:100%;'><div id='gmap_canvas' style='height:400px;width:100%;'></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div> <a style="position: absolute; bottom: 0; opacity: 0;" class="map-link" href='https://mapswebsite.net/'>www.mapswebsite.net</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=dd62fe68700869a2cb5f4a1ad0fd7e3b443109c7'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:12,scrollwheel: false,center:new google.maps.LatLng(42.6414853,-83.16893979999998),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(42.6414853,-83.16893979999998)});infowindow = new google.maps.InfoWindow({content:'<a target="_blank" href="https://www.google.com/maps/place/1935+Enterprise+Dr,+Rochester+Hills,+MI+48309/@42.6414991,-83.171129,17z/data=!3m1!4b1!4m5!3m4!1s0x8824c1f4da48dbb5:0x8478cf7668b2cdd4!8m2!3d42.6414991!4d-83.1689403"><strong>Dataspeed Inc.</strong><br>1935 Enterprise Drive<br>Rochester Hills, 48309<br></a>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
  </section>
