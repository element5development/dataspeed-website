<?php /*
DISPLAY LATEST CASE STUDIES WITH SPECIAL SCROLLING EFFECT
*/ ?>

  <section class="studies-preview slide-over-container clearfix">
    <div class="slide-one slide full-width clearfix">
      <div class="content-left one-half">
        <div class="sticky-slide-content">
            
            <?php 
              //PULL CONTENT OF LATEST FEATURED POST
              $args = array(
                'posts_per_page' => 4,
                'post_type'   => 'case-studies',
                'meta_query' => array(
                  array(
                    'key' => 'featured_content', 
                    'value' => '"featured"',
                    'compare' => 'LIKE'
                  )
                )
              );
              $the_query = new WP_Query( $args );
            ?>
            <?php if( $the_query->have_posts() ) { $i=0; ?>
              <?php while( $the_query->have_posts() ) : $the_query->the_post(); $i++; ?>
                <div id="study-preview-<?php echo $i ?>" class="studies-preview-content hidden animated <?php if ( $i == 1 ) { ?>selected visible<?php } ?>">
                  <p class="study-header">Case Studies</p>
                  <h2><?php the_title(); ?></h2>
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-study-challenge-white.svg" />
                  <h4>Background</h4>
                  <?php 
                    $challenge = get_field('the_challenge'); 
                    $challengeSmall = substr($challenge, 0, 250);
                  ?>
                  <p><?php echo $challengeSmall; ?>...</p>
                  <a href="/case-studies" class="secondary-button">Read the Rest</a>
                </div>

              <?php endwhile; ?>
            <?php } ?>
            <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>

        </div>
      </div>
      <div class="more-studies">
        <div class="max-width">
        
          <?php 
              //PULL CONTENT OF LATEST FEATURED POST
              $args = array(
                'posts_per_page' => 4,
                'post_type'   => 'case-studies',
                'meta_query' => array(
                  array(
                    'key' => 'featured_content', 
                    'value' => '"featured"',
                    'compare' => 'LIKE'
                  )
                )
              );
              $the_query = new WP_Query( $args );
            ?>
            <?php if( $the_query->have_posts() ) { $i = 0; ?>
              <?php while( $the_query->have_posts() ) : $the_query->the_post(); $i++; ?>
                <?php
                  //SET FEATURED IAMGE
                  if (has_post_thumbnail( $post->ID ) ) { 
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                  } else {
                    $image =  array( get_field('post_default', 'options'), "second");
                  }   
                ?>
                <div id="study-small-<?php echo $i ?>" class="study-small-preview one-fourth <?php if ( $i == 1 ) { ?>selected<?php } ?>" style="background-image: url(<?php echo $image[0]; ?>);">
                  <p><?php the_title(); ?></p>
                  <div class="dark-cover"></div>
                </div>


              <?php endwhile; ?>
            <?php } ?>
            <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>

        </div>
      </div>
    </div>
    <div class="slide-two slide full-width clearfix">
      <div class="content-right one-half">
        <?php 
          //PULL FEATURED IMAGE OF LATEST FEATURED POST
          $args = array(
            'posts_per_page' => 4,
            'post_type'   => 'case-studies',
            'meta_query' => array(
              array(
                'key' => 'featured_content', 
                'value' => '"featured"',
                'compare' => 'LIKE'
              )
            )
          );
          $the_query = new WP_Query( $args );
        ?>
        <?php if( $the_query->have_posts() ) { $i=0; ?>
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); $i++ ?>
            <?php
              //SET FEATURED IAMGE
              if (has_post_thumbnail( $post->ID ) ) { 
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
              } else {
                $image =  array( get_field('post_default', 'options'), "second");
              }   
            ?>
            <div id="study-image-<?php echo $i; ?>" class="sticky-slide-content hidden animated <?php if ($i == 1) { ?> selected visible<?php } ?>" style="background-image: url(<?php echo $image[0]; ?>);"></div>
          <?php endwhile; ?>
        <?php } ?>
        <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>

        </div>
      </div>
    </div>
  </section>
