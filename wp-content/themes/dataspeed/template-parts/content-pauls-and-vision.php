<?php /*
LEADER INTRODUCTION & VISION
*/ ?>

<?php $image = get_field('leader_intro_image'); ?>
<?php $imageLeft = get_field('left_background'); ?>
<?php $imageRight = get_field('right_background'); ?>

<section class="leader-intro-container full-width" style="background-image: url(<?php echo $image['url']; ?>);">
  <div class="leader-intro">
    <div class="green-border">
        <h2><?php the_field('leader_intro_header'); ?></h2>
        <p><?php the_field('leader_intro_content'); ?></p>
      </div>
    </div>
  </div>
</section>
<section class="vision full-width clearfix">
  <h2><?php the_field('vision_header'); ?></h2>
  <div class="one-half" style="background-image: url(<?php echo $imageLeft['url']; ?>);">
    <div class="vision-content right"><?php the_field('left_content'); ?></div>
  </div>
  <div class="one-half" style="background-image: url(<?php echo $imageRight['url']; ?>);">
    <div class="vision-content left"><?php the_field('right_content'); ?></div>
  </div>
</section>