<?php /*
SEARCH RESULT TEMPLATE
*/ ?>

  <?php  
    //SET FEATURED IAMGE
    if (has_post_thumbnail( $post->ID ) ) { 
      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
    } else {
      $image =  array( get_field('page_default', 'options'), "second");
    }
    //GET MAIN CATEGORY
    $categories = get_the_category();
  ?>
  <article class="post-preview search-result one-half dark-bg">
    <div class="post-top-half">
      <div class="search-result-image" style="background-image: url('<?php echo $image[0]; ?>');"></div>
    </div>
    <div class="post-contents">
      <h2 class="post-title"><?php the_title(); ?></h2>
      <div class="search-result-content"><?php the_excerpt(); ?></div>
      <a href="<?php the_permalink();?>" class="secondary-button">Read More</a>
    </div>
    <a class="link-cover" href="<?php the_permalink();?>"></a>
  </article>