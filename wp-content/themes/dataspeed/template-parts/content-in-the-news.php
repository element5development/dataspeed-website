<?php /*
DISPLAY LINKS TO EXTERNAL ARTICLES
*/ ?>

  <section class="in-the-news full-width clearfix">
    <div class="max-width">

      <h2>They Love Us.</h2>

      <?php if( have_rows('in_the_news') ) { $i=0;
        while ( have_rows('in_the_news') ) : $i++; the_row(); ?>

          <div class="one-third dark-bg">
            <?php $image = get_sub_field('post_image'); ?> 
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <h4 class="post-title"><?php the_sub_field('post_title'); ?></h4>
            <p class="post-date"><?php the_sub_field('post_date'); ?></p>
            <p class="post-description"><?php the_sub_field('post_description'); ?></p>
            <a href="<?php the_sub_field('post_link'); ?>" target="_blank" class="secondary-button">Read the article</a>
          </div>

        <?php endwhile;
          // no rows found
      } ?>
    </div>
  </section>
