<?php /*
LANDING PAGE CONTENTS
*/ ?>

<?php $imageTop = get_field('content_image_top'); ?>
<?php $imageBottom = get_field('content_image_bottom'); ?>

<section class="landing-contents full-width clearfix">
  <h2 class="landing-content-title"><?php the_field('content_header'); ?></h2>
  <div class="vertical-align-parent one-half <?php the_sub_field('product_header'); ?>">
    <div class="vertical-align">
      <div class="landing-content">
        <?php if(!empty( get_the_content() ) ) { ?>
          <div class="page-contents max-width">
            <?php the_content(); ?>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="one-half">
    <div class="landing-image" style="background-image: url(<?php echo $imageTop['url']; ?>);">
      <div class="vision-content right"><?php the_field('left_content'); ?></div>
    </div>
    <div class="landing-image" style="background-image: url(<?php echo $imageBottom['url']; ?>);">
      <div class="vision-content left"><?php the_field('right_content'); ?></div>
    </div>
  </div>
</section>