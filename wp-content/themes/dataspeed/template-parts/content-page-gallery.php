<?php /*
DISPLAY SLIDER GALLERY
*/ ?>

<section class="gallery full-width">
  <div class="gallery-cover">
    <div id="gallery" class="my-flipster max-width">
      <ul>  
        <?php $images = get_field('gallery'); ?>
        <?php if( $images ) { ?>
          <?php foreach( $images as $image ): ?>
            <li>
              <a class="fancybox" href="<?php echo $image['url']; ?>" data-fancybox-group="gallery">
                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                <div class="gallery-hover"></div>
              </a>
            </li>
          <?php endforeach; ?>
        <?php } ?>
      </ul>
    </div>
  </div>
</section>