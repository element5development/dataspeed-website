<?php /*
DISPLAY FEATURED IMAGE, BREADCRUMBS, PAGE TITLE
*/ ?>

<?php  //SET FEATURED IAMGE
if (has_post_thumbnail( $post->ID ) ) { 
  if ( is_post_type_archive( 'case-studies' ) ) {
    $image =  array( get_field('studies_featured', 'options'), "second");
  } elseif ( is_post_type_archive( 'resources' ) || is_category( 'videos' ) || is_category( 'code-repository' ) || is_category( 'product-spec-sheets' ) ) {
    $image =  array( get_field('resource_featured', 'options'), "second");
  } elseif ( is_archive() || is_home() ) {
    $image =  array( get_field('news_featured', 'options'), "second");
  } elseif ( is_single() ) {
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  } else {
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  }
} elseif ( is_post_type_archive( 'case-studies' ) ) { 
  $image =  array( get_field('studies_featured', 'options'), "second");
} elseif ( is_post_type_archive( 'resources' ) || is_category( 'videos' ) || is_category( 'code-repository' ) || is_category( 'product-spec-sheets' ) ) { 
  $image =  array( get_field('resource_featured', 'options'), "second");
} elseif ( is_archive() || is_home() ) { 
  $image =  array( get_field('news_featured', 'options'), "second");
} elseif ( is_single() ) { 
  $image =  array( get_field('post_default', 'options'), "second");
}else {
  $image =  array( get_field('page_default', 'options'), "second");
}
?>

<?php if (  is_page_template( 'template-styleguide.php' ) ) { ?>

<?php } elseif (  is_page_template( 'template-landing.php' ) ) { ?>

  <section class="page-top landing-top clearfix"  style="background-image: url('<?php echo $image[0]; ?>')">
    <div class="above-overlay">
      <div class="vertical-align-parent">
        <div class="vertical-align">
          <?php the_field('cover_title'); ?>
        </div>
      </div>
      <div class="down-arrow-pulse pulse"></div>
      <div class="down-arrow"></div>
    </div>
    <div class="color-overlay"></div>
    <div class="video-wrap"> 
      <video muted="" autoplay="" loop="" poster="<?php echo $image[0]; ?>" class="bgvid"> 
        <source src="<?php bloginfo('stylesheet_directory'); ?>/video/dataspeed-landing.mp4" type="video/mp4"> 
        <source src="<?php bloginfo('stylesheet_directory'); ?>/video/dataspeed-landing.webm" type="video/webm"> 
      </video> 
    </div> 
  </section>

<?php } elseif ( is_front_page() ) { ?>

  <section class="page-top home-top clearfix"  style="background-image: url('<?php echo $image[0]; ?>')">
    <div class="above-overlay">
      <div class="vertical-align-parent">
        <div class="vertical-align">
          <h1><?php the_field('cover_title'); ?></h1>
        </div>
      </div>
      <div class="down-arrow-pulse pulse"></div>
      <div class="down-arrow"></div>
    </div>
    <div class="color-overlay"></div>
    <div class="video-wrap"> 
      <video muted="" autoplay="" loop="" poster="<?php echo $image[0]; ?>" class="bgvid"> 
        <source src="<?php bloginfo('stylesheet_directory'); ?>/video/dataspeed-landing.mp4" type="video/mp4"> 
        <source src="<?php bloginfo('stylesheet_directory'); ?>/video/dataspeed-landing.webm" type="video/webm"> 
      </video> 
    </div> 
  </section>

<?php } elseif ( is_post_type_archive( 'case-studies' ) ) { ?>

  <section class="page-top clearfix">
    <div class="one-half page-info">
      <div class="breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      </div>
      <div class="page-title">
        <h1>Case Studies</h1>
      </div>
    </div>
    <div class="one-half page-image" style="background-image: url('<?php echo $image[0]; ?>')">
    </div>
  </section>

<?php } elseif ( is_post_type_archive( 'resources' ) || is_category( 'videos' ) || is_category( 'code-repository' ) || is_category( 'product-spec-sheets' ) ) { ?>

  <section class="page-top clearfix">
    <div class="one-half page-info">
      <div class="breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      </div>
      <div class="page-title">
        <h1>Resources</h1>
      </div>
    </div>
    <div class="one-half page-image" style="background-image: url('<?php echo $image[0]; ?>')">
    </div>
  </section>

<?php } elseif ( is_archive() || is_home() ) { ?>

  <section class="page-top clearfix">
    <div class="one-half page-info">
      <div class="breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      </div>
      <div class="page-title">
        <h1>Dataspeed Inc. in the News</h1>
      </div>
    </div>
    <div class="one-half page-image" style="background-image: url('<?php echo $image[0]; ?>')">
    </div>
  </section>

<?php } elseif ( is_single() ) { ?>

  <section class="page-top post-top clearfix">
    <div class="full-width page-image" style="background-image: url('<?php echo $image[0]; ?>')">
      <div class="breadcrumbs max-width">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      </div>
    </div>
  </section>

<?php } elseif ( is_page(345) ) { ?>

  <section class="page-top clearfix">
    <div class="one-half page-info">
      <div class="breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      </div>
      <div class="page-title">
        <h1><?php the_title() ?></h1>
        <p><?php the_field('address', 'options') ?></p>
        <p>
          <a class="phone" href="tel:+1-<?php the_field('phone', 'options') ?>">OFFICE: +1 <?php the_field('phone', 'options') ?></a><br/>
          <a class="fax">FAX: +1 <?php the_field('fax', 'options') ?></a>
        </p>
      </div>
    </div>
    <div class="one-half page-image" style="background-image: url('<?php echo $image[0]; ?>')">
    </div>
  </section>

<?php } elseif ( is_page(38) ) { //WHO WE ARE?>
  
  <section class="page-top clearfix">
    <div class="one-half page-info">
      <div class="breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      </div>
      <div class="page-title">
        <h1><?php the_title() ?></h1>
        <h2>We bring you the future you were always promised.</h2>
      </div>
    </div>
    <div class="one-half page-image" style="background-image: url('<?php echo $image[0]; ?>')">
    </div>
  </section>

<?php } elseif ( is_page(685) ) { //INVESTORS ?>
  
  <section class="page-top clearfix">
    <div class="one-half page-info">
      <div class="breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      </div>
      <div class="page-title">
        <h1>We make robots move and cars drive themselves <span>Invest in the future you’ve always dreamt about.</span></h1>
      </div>
    </div>
    <div class="one-half page-image" style="background-image: url('<?php echo $image[0]; ?>')">
    </div>
  </section>

<?php } else { ?>

  <section class="page-top clearfix">
    <div class="one-half page-info">
      <div class="breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
      </div>
      <div class="page-title">
        <h1><?php the_title() ?></h1>
        <p><?php if ( is_page(660) ) {
          if (isset($_GET['job-title'])) {
            echo $_GET['job-title'];
          }
        } ?></p>
      </div>
    </div>
    <div class="one-half page-image" style="background-image: url('<?php echo $image[0]; ?>')">
    </div>
  </section>

<?php } ?>