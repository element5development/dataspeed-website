<?php /*
SORTABLE TABLE OF JOB OPENINGS
*/ ?>
<section class="openings full-width">
  <div class="max-width">
    <h2>Current <b>Openings</b></h2>
    <table id="jobs-table" class="tablesorter">
      <thead>
        <tr>
          <th>Position</th>
          <th>Type</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>

      <?php 
        $args = array( 'post_type' => 'career', 'posts_per_page' => -1 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
      ?>
          <tr>
            <td><a href="<?php echo get_permalink() ?>"><?php the_title(); ?><p class="legal-text"><?php the_field('job_type'); ?></p></a></td>
            <td><?php the_field('employement_type'); ?></td>
            <td><?php the_field('location'); ?></td>
          </tr>
      <?php endwhile; ?>

      </tbody>
    </table>

    <div class="mobile-openings">
      <?php 
        $args = array( 'post_type' => 'career', 'posts_per_page' => -1 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
      ?>

          <div class="position">
            <a href="<?php echo get_permalink() ?>">
              <p><span>Position: </span><?php the_title(); ?></p>
              <p><span>Type: </span><?php the_field('employement_type'); ?></p>
              <p><span>Location: </span><?php the_field('location'); ?></p>
            </a>
          </div>

      <?php endwhile; ?>
    </div>



  </div>
</section>