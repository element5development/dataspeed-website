<?php /*
CSS PULLED FROM ACF STYLEGUIDE
*/ ?>


<style>
  html, body { font-size: <?php the_field('base_font_size', 'option'); ?>px !important; }
  h1, .widget h1  { 
    font-size: <?php the_field('h1_font_size', 'option'); ?>em;
    line-height: <?php the_field('h1_line_height', 'option'); ?>%;
    font-weight: <?php the_field('h1_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('h1_character_spacing', 'option'); ?>px;
    color: <?php the_field('h1_color', 'option'); ?>; 
  } 
  h2, .widget h2  { 
    font-size: <?php the_field('h2_font_size', 'option'); ?>em;
    line-height: <?php the_field('h2_line_height', 'option'); ?>%;
    font-weight: <?php the_field('h2_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('h2_character_spacing', 'option'); ?>px;
    color: <?php the_field('h2_color', 'option'); ?>; 
  } 
  h3, .widget h3  { 
    font-size: <?php the_field('h3_font_size', 'option'); ?>em;
    line-height: <?php the_field('h3_line_height', 'option'); ?>%;
    font-weight: <?php the_field('h3_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('h3_character_spacing', 'option'); ?>px;
    color: <?php the_field('h3_color', 'option'); ?>; 
  } 
  h4, .widget h4  { 
    font-size: <?php the_field('h4_font_size', 'option'); ?>em;
    line-height: <?php the_field('h4_line_height', 'option'); ?>%;
    font-weight: <?php the_field('h4_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('h4_character_spacing', 'option'); ?>px;
    color: <?php the_field('h4_color', 'option'); ?>; 
  } 
  h5, .widget h5  { 
    font-size: <?php the_field('h5_font_size', 'option'); ?>em;
    line-height: <?php the_field('h5_line_height', 'option'); ?>%;
    font-weight: <?php the_field('h5_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('h5_character_spacing', 'option'); ?>px;
    color: <?php the_field('h5_color', 'option'); ?>; 
  } 
  h6, .widget h6  { 
    font-size: <?php the_field('h6_font_size', 'option'); ?>em;
    line-height: <?php the_field('h6_line_height', 'option'); ?>%;
    font-weight: <?php the_field('h6_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('h6_character_spacing', 'option'); ?>px;
    color: <?php the_field('h6_color', 'option'); ?>; 
  } 
  p {
    font-size: <?php the_field('p_font_size', 'option'); ?>em;
    line-height: <?php the_field('p_line_height', 'option'); ?>%;
    font-weight: <?php the_field('p_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('p_character_spacing', 'option'); ?>px;
    color: <?php the_field('p_color', 'option'); ?>; 
  }
  p, ol, ul, nav a { color: <?php the_field('text_color', 'option'); ?>; }
  a { color: <?php the_field('cta_color', 'option'); ?>; ; }
  blockquote p, p.block-quote {
    font-size: <?php the_field('bq_font_size', 'option'); ?>em;
    line-height: <?php the_field('bq_line_height', 'option'); ?>%;
    font-weight: <?php the_field('bq_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('bq_character_spacing', 'option'); ?>px;
    color: <?php the_field('bq_color', 'option'); ?>; 
  }
  blockquote { 
    background-image: url(<?php the_field('block_quote_icon', 'option'); ?>); 
  }
  hr { background-color: <?php the_field('cta_color', 'option'); ?>; }
  ul li { background-image: url(<?php the_field('bullet_icon', 'option'); ?>); }
  .primary-button, form input[type="submit"], .search-submit { 
    background-color: <?php the_field('cta_color', 'option'); ?>; 
    border-color: <?php the_field('cta_color', 'option'); ?>; 
    font-size: <?php the_field('primary_font_size', 'option'); ?>em;
    line-height: <?php the_field('primary_line_height', 'option'); ?>%;
    font-weight: <?php the_field('primary_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('primary_character_spacing', 'option'); ?>px;
    color: <?php the_field('primary_color', 'option'); ?>; 
  }
  .primary-button:hover, form input[type="submit"]:hover, .search-submit:hover { 
    background-color: <?php the_field('hover_color', 'option'); ?>; 
    border-color: <?php the_field('hover_color', 'option'); ?>; 
  }
  .primary-button.arrow { background-image: url(<?php the_field('arrow_primary_icon', 'option'); ?>); }
  .secondary-button { 
    border-color: <?php the_field('secondary_cta_color', 'option'); ?>; 
    color: <?php the_field('secondary_cta_color', 'option'); ?>;
    font-size: <?php the_field('secondary_font_size', 'option'); ?>em;
    line-height: <?php the_field('secondary_line_height', 'option'); ?>%;
    font-weight: <?php the_field('secondary_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('secondary_character_spacing', 'option'); ?>px;
    color: <?php the_field('secondary_color', 'option'); ?>; 
  }
  .secondary-button:hover { 
    border-color: <?php the_field('secondary_hover_color', 'option'); ?>; 
    color: <?php the_field('secondary_hover_color', 'option'); ?>;
  }
  .secondary-button.arrow { background-image: url(<?php the_field('arrow_secondary_icon', 'option'); ?>); }
  .dark-bg { background-color: <?php the_field('dark_background', 'option'); ?>; }
  .dark-bg * { color: #fff; }
  .dark-bg .primary-button { 
    font-size: <?php the_field('primary_dk_font_size', 'option'); ?>em;
    line-height: <?php the_field('primary_dk_line_height', 'option'); ?>%;
    font-weight: <?php the_field('primary_dk_font_weight', 'option'); ?>;
    letter-spacing: <?php the_field('primary_dk_character_spacing', 'option'); ?>px;
    color: <?php the_field('primary_dk_color', 'option'); ?>; 
  }
  .sub-section-nav { border-bottom: 2px solid <?php the_field('cta_color', 'option'); ?>; }
  .facebook.social-link { background-image: url(<?php the_field('facebook', 'option'); ?>); }
  .instagram.social-link { background-image: url(<?php the_field('instagram', 'option'); ?>); }
  .twitter.social-link { background-image: url(<?php the_field('twitter', 'option'); ?>); }
  .pinterest.social-link { background-image: url(<?php the_field('pinterest', 'option'); ?>); }
  .linkedin.social-link { background-image: url(<?php the_field('linkedin', 'option'); ?>); }
  .youtube.social-link { background-image: url(<?php the_field('youtube', 'option'); ?>); }
</style>