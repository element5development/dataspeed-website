<?php /*
CLIENT POST FEED
*/ ?>

<section class="client-posts full-width clearfix">
  <div class="max-width">
    <h2>Everyone uses our kits. Who?<br/>Glad you asked.</h2>
    <?php echo do_shortcode('[ajax_load_more post_type="clients" posts_per_page="3"]'); ?>
  </div>
</section>
