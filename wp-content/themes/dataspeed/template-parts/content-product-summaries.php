<?php /*
DISPLAY PERODUCT IMAGE, TEXT SUMMARY AND SPEC FILE DOWNLOAD
*/ ?>

  <section class="product-summaries clearfix">
    <?php if( have_rows('products') ) { $i = 0;
      while ( have_rows('products') ) : $i++; the_row(); ?>

          <div id="product-<?php echo $i; ?>" class="product-container clearfix">
            <?php $image = get_sub_field('product_image'); ?> 
            <div class="product-image one-half <?php the_sub_field('product_header'); ?>" style="background-image: url(<?php echo $image['url']; ?>)"></div>
            <div class="vertical-align-parent one-half <?php the_sub_field('product_header'); ?>">
              <div class="vertical-align">
                <div class="product-content">
                  <p class="product-header"><?php the_sub_field('product_header'); ?></p>
                  <h2><?php the_sub_field('product_title'); ?></h2>
                  <p><?php the_sub_field('product_description'); ?></p>
                  <?php if ( get_sub_field('product_specs') ) { ?>
                    <a href="<?php the_sub_field('product_specs'); ?>" class="primary-button">Product Specs</a>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
 
      <?php endwhile;
    } else {
      // no rows found
    } ?>
  </section>
