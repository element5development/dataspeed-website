<?php /*
DISPLAY INSPERATION QUOTE
*/ ?>

  <section class="insperational full-width clearfix">
    <div class="bubble left">
      <div class="green-border insperational-quote">
      </div>
    </div>
    <div class="bubble">
      <div class="green-border insperational-quote">
        <?php if ( is_page(685) ) { ?>
          <h2>The future we were always promised.</h2>
          <a href="http://dataspeedinc.com/contact/" class="secondary-button">Work With Us</a>
          <a href="tel:+12488790528">+1 (248) 879-0528</a>
        <?php } else { ?>
          <h2><?php the_field('inspirational_qutoe'); ?></h2>
        <?php } ?>
      </div>
    </div>
    <div class="bubble right">
      <div class="green-border insperational-quote">
      </div>
    </div>
  </section>
