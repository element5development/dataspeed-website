/* FLIPSTER DECLARATIONS */

jQuery("#flat").flipster({
  start: 'center',
  loop: true,
  autoplay: 2000,
  style: 'flat',
  spacing: 0
});
jQuery("#gallery").flipster({
  start: 'center',
  loop: true,
  autoplay: false,
  style: 'flat',
  spacing: 0,
  click: false,
  scrollwheel: false,
  buttons: 'custom',
  buttonPrev: '',
  buttonNext: '',
});
jQuery("#driverless-videos").flipster({
  start: 0,
  loop: true,
  autoplay: false,
  style: 'flat',
  spacing: 0,
  click: false,
  scrollwheel: false,
  buttons: 'custom',
  buttonPrev: '',
  buttonNext: '',
});
jQuery("#robot-videos").flipster({
    start: 0,
    loop: true,
    autoplay: false,
    style: 'flat',
    spacing: 0,
    click: false,
    scrollwheel: false,
    buttons: 'custom',
    buttonPrev: '',
    buttonNext: '',
});