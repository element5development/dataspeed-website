/* ENTRANCE ANIMATIONS */

jQuery(document).ready(function() {

  var window_width = jQuery( window ).width();
  if (window_width < 750) {
    //DISABLE FADE OUT ON MOBILE
  } else {
    //FADE OUT PAGE TITLE FOR SCROLL EFFECT
    $(window).scroll(function() {    
      var scroll = $(window).scrollTop();
      if (scroll >= 375) {
        $(".home-top .above-overlay").addClass("fadeOut");
      } else {
        $(".home-top .above-overlay").removeClass("fadeOut");
      }
    }); 
    //FADE OUT INSPERATIONAL BUBBLES FOR SCROLL EFFECT
    $(window).scroll(function() {    
      var scroll = $(window).scrollTop();
      if (scroll >=  $(document).height()*0.32) {
        $(".home .insperational .bubble").addClass("fadeOut");
      } else {
        $(".home .insperational .bubble").removeClass("fadeOut");
      }
    }); 
    //FADE OUT MAKE REVIEW BUBBLES FOR SCROLL EFFECT
    $(window).scroll(function() {    
      var scroll = $(window).scrollTop();
      if (scroll >=  $(document).height()*0.55) {
        $(".home .make-preview .max-width").addClass("fadeOut");
      } else {
        $(".home .make-preview .max-width").removeClass("fadeOut");
      }
    });
  } 
});