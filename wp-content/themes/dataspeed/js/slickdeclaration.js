/* SLICK SLIDER DECLARATIONS */

$(document).ready(function(){
  $('#infinite-logos').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: false,
    infinite: true,
    speed: 300,
    rows: 1,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
        { breakpoint: 1025, settings: {
            rows: 1,
            slidesToShow: 3,
            slidesToScroll: 1
        }},
        { breakpoint: 750, settings: {
            rows: 1,
            slidesToShow: 1,
            slidesToScroll: 1
        }}
    ]
  });
});