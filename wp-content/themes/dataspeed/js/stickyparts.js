  /* STICKY DIVS */
window.setInterval(function(){
jQuery(document).ready(function(){
  jQuery("#sticky-nav").stick_in_parent({ parent: 'body' });
  jQuery("#styleguide-sidebar").stick_in_parent({ offset_top: 125 });
  jQuery(".sticky-service").stick_in_parent({ offset_top: 0 });
  jQuery(".product-image").stick_in_parent({ offset_top: 0 });
  jQuery(".home .sticky-study").stick_in_parent({ offset_top: 0 });
  jQuery(".sticky-slide-content").stick_in_parent({ offset_top: 100 });
  jQuery(".home .insperational").stick_in_parent({ offset_top: 70 });
  jQuery(".home .make-preview").stick_in_parent({ offset_top: 70 });
  jQuery(".slide-effect-4  .logo-slider").stick_in_parent({ offset_top: 70 });

//DISABLE STICKY PARTS ON MOBILE
  var window_width = jQuery( window ).width();
  if (window_width < 750) {
    jQuery("#sticky-nav").trigger("sticky_kit:detach");
    jQuery(".sticky-slide-content").trigger("sticky_kit:detach");
    jQuery(".home .insperational").trigger("sticky_kit:detach");
    jQuery(".home .make-preview").trigger("sticky_kit:detach");
    jQuery(".slide-effect-4  .logo-slider").trigger("sticky_kit:detach");
    jQuery(".sticky-service").trigger("sticky_kit:detach");
    jQuery(".product-image").trigger("sticky_kit:detach");
    jQuery(".home .sticky-study").trigger("sticky_kit:detach");
  } else {
    //nothing
  }
  jQuery( window ).resize(function() {
    window_width = jQuery( window ).width();
    if (window_width < 750) {
      jQuery("#sticky-nav").trigger("sticky_kit:detach");
      jQuery(".sticky-slide-content").trigger("sticky_kit:detach");
      jQuery(".home .insperational").trigger("sticky_kit:detach");
      jQuery(".home .make-preview").trigger("sticky_kit:detach");
      jQuery(".slide-effect-4  .logo-slider").trigger("sticky_kit:detach");
      jQuery(".sticky-service").trigger("sticky_kit:detach");
      jQuery(".product-image").trigger("sticky_kit:detach");
      jQuery(".home .sticky-study").trigger("sticky_kit:detach");
    } else {
      //nothing
    }
  });




});
}, 1000);