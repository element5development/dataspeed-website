<?php /* Template Name: Woocommerce Page Template */ ?>

<?php get_header(); ?>

<main class="full-width">

	<!-- ADD PAGE CONTENT -->
    <?php if(!empty( get_the_content() ) ) { ?>
  		<div class="page-contents max-width">
  			<?php the_content(); ?>
  		</div>
    <?php } ?>

</main>

<?php get_footer(); ?>