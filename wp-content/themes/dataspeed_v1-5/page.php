<?php /*
DEFAULT PAGE TEMPLATE
*/ ?>

<?php get_header(); ?>

<main class="full-width">

  <!-- PAGE TITLE, FEATURED IMAGE, BREADCRUMBS -->
    <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- LEADER INTRODUCTION & VISION -->
    <?php if ( get_field('leader_intro_header') ) {
      get_template_part( 'template-parts/content', 'pauls-and-vision' );
    } ?>

  <!-- PAGE CONTENT WITH A BACKGROUND IMAGE -->
    <?php if ( get_field('background_image') ) {
      get_template_part( 'template-parts/content', 'page-content-background' );
    } ?>

  <!-- PRODUCTS -->
    <?php if ( have_rows('products') ) { 
      get_template_part( 'template-parts/content', 'product-summaries' );
    } ?>

  <!-- PRODUCTS -->
    <?php if ( have_rows('service') ) { 
      get_template_part( 'template-parts/content', 'service-summaries' );
    } ?>

  <!-- PAGE GALLERY -->
    <?php if ( get_field('gallery') ) {
      get_template_part( 'template-parts/content', 'page-gallery' );
    } ?>

	<!-- ADD PAGE CONTENT -->
    <?php if(!empty( get_the_content() ) ) { ?>
  		<div class="page-contents max-width">
  			<?php the_content(); ?>
  		</div>
    <?php } ?>

  <!-- DISPLAY CAREERS TABLE & LINKS -->
    <?php if ( is_page(357) ) {
      $args = array( 'post_type' => 'career', 'posts_per_page' => -1 );
      $loop = new WP_Query( $args );
      if ( $loop->have_posts() ) {
        get_template_part( 'template-parts/content', 'openings' ); //DISPLAY CAREERS TABLE
      } else {
        get_template_part( 'template-parts/content', 'openings-none' ); //DISPLAY NO OPENINGS TEXT
      }
    } ?>

  <!-- SUPPORTERS LOGO SLIDER -->
    <?php get_template_part( 'template-parts/content', 'logo-slider' ); ?>

  <!-- WHITE PAPER DOWNLOADS -->
    <?php get_template_part( 'template-parts/content', 'white-papers' ); ?>

</main>

<?php get_footer(); ?>