<?php /*
DISPLAY ROBOT VIDEOS WITH SPECIAL SCROLLING EFFECT
*/ ?>

  <section class="robot-preview slide-over-container clearfix">
    <div class="slide-one slide full-width clearfix">
      <div class="content-left one-half">
        <div class="sticky-slide-content">
          <p class="robot-header">What we do</p>
          <h2>Robots that move</h2>
          <div id="robot-videos" class="my-flipster max-width">
            <ul>
              <?php if( have_rows('robot_videos') ) {
                  while ( have_rows('robot_videos') ) : the_row(); ?>

                      <li>
                        <a class="fancybox-media" href="https://www.youtube.com/watch?v=<?php the_sub_field('video_ID'); ?>?fs=1&amp;autoplay=1;">
                          <div class="video-screen-container">
                            <img class="driverless-video-imge" src="https://img.youtube.com/vi/<?php the_sub_field('video_ID'); ?>/maxresdefault.jpg" alt="Dataspeed Video: Robots that move" />
                            <div class="dark-overlay"></div>
                          </div>
                          <h4><?php the_sub_field('video_title'); ?></h4>
                          <p><?php the_sub_field('video_description'); ?></p>
                        </a>
                      </li>

                  <?php endwhile;
              } else {
                  // no rows found
              } ?>
            </ul>
          </div>
          <a href="<?php the_permalink(356);?>/#product-1" class="primary-button">Show me more</a>
        </div>
      </div>
    </div>
    <div class="slide-two slide full-width clearfix">
      <div class="content-right one-half">
        <div class="sticky-slide-content">
          <?php $image = get_field('robot_background_image'); ?>
          <div class="sticky-slide-content" style="background-image: url('<?php echo $image['url']; ?>');"></div>
        </div>
      </div>
    </div>
  </section>
