<?php /*
DISPLAY PRODUCT PREVIEWS
*/ ?>

  <section class="make-preview full-width clearfix">
    <div class="vertical-align-parent">
      <div class="vertical-align">
        <div class="max-width clearfix">
          <p class="make-header">What we make</p>
          <?php if( have_rows('what_we_make_preview') ) { $i=0;
            while ( have_rows('what_we_make_preview') ) : $i++; the_row(); ?>

              <?php $image = get_sub_field('product_image'); ?> 
              <div class="one-third">
                <a class="fancybox-media" href="<?php the_sub_field('product_video'); ?>?fs=1&amp;autoplay=1">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                  <div class="dark-cover"></div>
                </a>
                <p class="product-title"><?php the_sub_field('product_title'); ?></p>
                <p class="product-description"><?php the_sub_field('product_description'); ?></p>
                <a href="<?php the_permalink('356'); ?>/#product-<?php echo $i; ?>" class="secondary-button">Learn More</a>
              </div>

            <?php endwhile;
              // no rows found
          } ?>
        </div>
      </div>
    </div>
  </section>
