<?php /*
DISPLAY LATEST POSTS WITH SPECIAL SCROLLING EFFECT
*/ ?>

  <section class="latest-posts slide-over-container clearfix">
    <div class="slide-one slide full-width clearfix">
      <div class="content-left one-half">
        <?php 
          //PULL FEATURED IMAGE OF LATEST FEATURED POST
          $args = array(
            'posts_per_page' => 1,
            'post_type'   => 'post',
            'meta_query' => array(
              array(
                'key' => 'featured_content', 
                'value' => '"featured"',
                'compare' => 'LIKE'
              )
            )
          );
          $the_query = new WP_Query( $args );
        ?>
        <?php if( $the_query->have_posts() ) { ?>
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <?php
              //SET FEATURED IAMGE
              if (has_post_thumbnail( $post->ID ) ) { 
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
              } else {
                $image =  array( get_field('post_default', 'options'), "second");
              }   
            ?>
            <div class="sticky-slide-content" style="background-image: url(<?php echo $image[0]; ?>);">
          <?php endwhile; ?>
        <?php } ?>
        <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
        </div>
      </div>
    </div>
    <div class="slide-two slide full-width clearfix">
      <div class="content-right one-half dark-bg">
        <div class="sticky-slide-content">
          
            <?php 
              //PULL CONTENT OF LATEST FEATURED POST
              $args = array(
                'posts_per_page' => 1,
                'post_type'   => 'post',
                'meta_query' => array(
                  array(
                    'key' => 'featured_content', 
                    'value' => '"featured"',
                    'compare' => 'LIKE'
                  )
                )
              );
              $the_query = new WP_Query( $args );
            ?>
            <?php if( $the_query->have_posts() ) { ?>
              <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                
                <p class="news-header">Dataspeed Inc. News</p>
                <h2><?php the_title(); ?></h2>
                <p><?php the_date('F j, Y'); ?></p>
                <p><?php echo get_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" class="primary-button">Read the Rest</a>


              <?php endwhile; ?>
            <?php } ?>
            <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>

            <?php 
              //PULL CONTENT OF LATEST FEATURED POST
              $args = array(
                'posts_per_page' => 3,
                'post_type'   => 'post',
                'offset' => 1,
                'meta_query' => array(
                  array(
                    'key' => 'featured_content', 
                    'value' => '"featured"',
                    'compare' => 'LIKE'
                  )
                )
              );
              $the_query = new WP_Query( $args );
            ?>
            <?php if( $the_query->have_posts() ) { ?>
              <ul>
                <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                  <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile; ?>
              </ul>
            <?php } ?>
            <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>

        </div>
      </div>
    </div>
  </section>
