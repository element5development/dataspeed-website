<?php /*
DISPLAY SUPPORTS LOGOS IN A SLIDER FORMAT
*/ ?>

  <section class="logo-slider full-width">
    <div class="vertical-align-parent">
      <div class="vertical-align">
        <h2>Everyone uses our kits.</h2>
        <p>WHO? GLAD YOU ASKED.</p>
        <div id="infinite-logos" class="max-width">
            <?php if( have_rows('supporters', 'options') ) { ?>
              <?php while ( have_rows('supporters', 'options') ) : the_row(); ?>
                
                <?php $image = get_sub_field('supporters_logo'); ?>
                <?php if( !empty($image) ) { ?> 
                 <div class="vertical-align-parent"><div class="vertical-align"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div></div>
                <?php } ?>

              <?php endwhile; ?>
            <?php } else {
              // no rows found
            } ?>
        </div>
      </div>
    </div>
  </section>
