<?php /*
LEADER INTRODUCTION & VISION
*/ ?>

<?php $image = get_field('leader_intro_image'); ?>
<?php $imageLeft = get_field('left_background'); ?>
<?php $imageRight = get_field('right_background'); ?>

<section class="leader-intro-container full-width" style="background-image: url(<?php echo $image['url']; ?>);">
  <div class="one-half">
    <div class="leader-intro">
      <div class="green-border">
        <h2><?php the_field('leader_intro_header'); ?></h2>
        <p><?php the_field('leader_intro_content'); ?></p>
      </div>
    </div>
  </div>
</section>

<!--<section class="meet-the-team full-width clearfix">-->
<!--  <h2>Meet the team</h2>-->
<!--</section>-->
<!---->
<!--<section class="meet-the-team-leadership full-width clearfix">-->
<!--  <div class="leadership-inner">-->
<!--    <div class="leadership-headline">-->
<!--      <h2>Leadership</h2>-->
<!--    </div>-->
<!--    <ul>-->
<!--      --><?php
//      $loop = new WP_Query( array(
//        'post_type' => 'teammember',
//        'cat' => 79,
//        'posts_per_page' => 3,
//        'orderby' => 'date',
//        'order' => 'ASC'
//      ) ); ?>
<!--      --><?php //while ( $loop->have_posts() ) : $loop->the_post();?>
<!--        <li class="leader-member one-third">-->
<!--          <div class="leader-member-img">-->
<!--            <img src="--><?php //the_field('team_member_image'); ?><!--" alt="Leader Image">-->
<!--          </div>-->
<!--          <div class="leader-name">-->
<!--            <h3>--><?php //the_field('team_member_name'); ?><!--</h3>-->
<!--          </div>-->
<!--          <div class="leader-title">-->
<!--            <h4>--><?php //the_field('team_member_position'); ?><!--</h4>-->
<!--          </div>-->
<!--        </li>-->
<!--      --><?php //endwhile; ?>
<!--    </ul>-->
<!--  </div>-->
<!--</section>-->

<!--Client may re-add at a later date!!-->
<!--<section class="meet-the-team-engineers full-width clearfix">-->
<!--  <div class="engineers-inner">-->
<!--    <div class="engineers-headline">-->
<!--      <h2>Engineering</h2>-->
<!--    </div>-->
<!--    <ul>-->
<!--      --><?php
//      $loop = new WP_Query( array(
//        'post_type' => 'teammember',
//        'cat' => 80,
//        'posts_per_page' => 9999999,
//        'orderby' => 'date',
//        'order' => 'ASC'
//      ) ); ?>
<!--      --><?php //while ( $loop->have_posts() ) : $loop->the_post();?>
<!--        <li class="eng-member one-fourth">-->
<!--          <div class="eng-member-img">-->
<!--            <img src="--><?php //the_field('team_member_image'); ?><!--" alt="Engineer Image">-->
<!--          </div>-->
<!--          <div class="eng-name">-->
<!--            <h3>--><?php //the_field('team_member_name'); ?><!--</h3>-->
<!--          </div>-->
<!--        </li>-->
<!--      --><?php //endwhile; ?>
<!--    </ul>-->
<!--  </div>-->
<!--</section>-->

<!--<div class="footnote clearfix">-->
<!--  <div class="footnote-contain one-fifth">-->
<!--    <p>PhD.* Candidate</p>-->
<!--  </div>-->
<!--</div>-->
<!---->
<!--<section class="meet-the-team-support full-width clearfix">-->
<!--  <div class="support-inner">-->
<!--    <div class="support-headline">-->
<!--      <h2>Support</h2>-->
<!--    </div>-->
<!--    <ul>-->
<!--      --><?php
//      $loop = new WP_Query( array(
//        'post_type' => 'teammember',
//        'cat' => 81,
//        'posts_per_page' => 9999999,
//        'orderby' => 'date',
//        'order' => 'ASC'
//      ) ); ?>
<!--      --><?php //while ( $loop->have_posts() ) : $loop->the_post();?>
<!--        <li class="support-member one-fourth">-->
<!--          <div class="eng-member-img">-->
<!--            <img src="--><?php //the_field('team_member_image'); ?><!--" alt="Support Image">-->
<!--          </div>-->
<!--          <div class="support-name">-->
<!--            <h3>--><?php //the_field('team_member_name'); ?><!--</h3>-->
<!--          </div>-->
<!--        </li>-->
<!--      --><?php //endwhile; ?>
<!--    </ul>-->
<!--  </div>-->
<!--</section>-->

<section class="vision full-width clearfix">
  <h2><?php the_field('vision_header'); ?></h2>
  <div class="one-half" style="background-image: url(<?php echo $imageLeft['url']; ?>);">
    <div class="vision-content right"><?php the_field('left_content'); ?></div>
  </div>
  <div class="one-half" style="background-image: url(<?php echo $imageRight['url']; ?>);">
    <div class="vision-content left"><?php the_field('right_content'); ?></div>
  </div>
</section>