<?php /*
DISPLAY SUPPORTS LOGOS IN A SLIDER FORMAT
*/ ?>

<?php $image = get_field('background_image'); ?>

<section class="background-image-content full-width" style="background-image: url(<?php echo $image['url']; ?>);">
  <div class="max-width">
    <?php the_field('background_content'); ?>
  </div>
</section>