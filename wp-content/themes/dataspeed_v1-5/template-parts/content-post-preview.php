<?php /*
THIS CODE IS NOT USED BUT A TEMPLATE USED WITHIN THE AJAX LOAD MORE PLUGIN
*/ ?>


<?php if ( 'case-studies' == get_post_type() ) { //CASE STUDIES TEMPALTE ?>
  <?php  
    //SET FEATURED IAMGE
    if (has_post_thumbnail( $post->ID ) ) { 
      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); 
    } else {
      $image =  array( get_field('post_default', 'options'), "second");
    }
  ?>
  <article id="study-<?php echo get_the_ID(); ?>" class="case-study full-width clearfix">
    <div class="one-half sticky-study study-image" style="background-image: url(<?php echo $image[0]; ?>);"></div>
    <div class="one-half study-content-container dark-bg">
      <div class="study-content">
        <p class="study-header">Case Study</p>
        <h2><?php the_title(); ?></h2>
        <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-study-challenge.svg" />
        <h4>Background</h4>
        <?php the_field('the_challenge'); ?>
        <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-study-solution.svg" />
        <h4>The Solution</h4>
        <?php the_field('the_solution'); ?>
        <img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-study-results.svg" />
        <h4>The Results</h4>
        <?php the_field('the_results'); ?>
        <?php if ( get_field('link_download') ) { ?>
          <a href="<?php the_field('link_download'); ?>" class="primary-button">View Demo</a>
        <?php } ?>
      </div>
    </div>
  </article>
<?php } elseif ( 'clients' == get_post_type() ) { //CLIENT TEMPLATE ?>
  <article class="post-preview client-post clearfix">
    <div class="one-third"><?php the_post_thumbnail(); ?></div>
    <div class="post-contents two-third">
      <h4 class="post-title"><?php the_title(); ?></h4>
      <?php the_field('testimony'); ?>
  </article>
<?php } elseif ( 'resources' == get_post_type() ) { //RESOURCE TEMPALTE ?>
  <?php  
    //GET MAIN CATEGORY
    $categories = get_the_category();
  ?>
  <article class="post-preview one-half dark-bg">
    <div class="post-top-half">
      <div class="post-category"><?php echo esc_html( $categories[0]->name ); ?></div>
     <?php   
      //SET FEATURED IAMGE
      if (has_post_thumbnail( $post->ID ) ) { 
        the_post_thumbnail();
      } else {
        $image =  array( get_field('post_default', 'options'), "second");?>
        <img src="<?php echo $image[0]; ?>" alt="<?php echo $image['alt']; ?>" />
      <?php } ?>
    </div>
    <div class="post-contents">
      <h2 class="post-title"><?php the_title(); ?></h2>
      <?php if ( get_field('video_link') ) { ?>
        <a class="secondary-button">View Video</a>
      <?php } else { ?>
        <a class="secondary-button">Download</a>
      <?php } ?>
    </div>
    <?php if ( get_field('video_link') ) { ?>
      <a class="fancybox-media link-cover" href="<?php the_field('video_link');?>?fs=1&amp;autoplay=1"></a>
    <?php } elseif ( get_field('url_link') ) { ?>
      <a href="<?php the_field('url_link');?>" class="link-cover"></a>
    <?php } else { } ?>
  </article>
<?php } else { //POST TEMPLATE ?> 
  <?php  
    //GET MAIN CATEGORY
    $categories = get_the_category();
  ?>
  <article class="post-preview one-half dark-bg">
    <div class="post-top-half">
      <div class="post-category"><?php echo esc_html( $categories[0]->name ); ?></div>
      <?php   
      //SET FEATURED IAMGE
      if (has_post_thumbnail( $post->ID ) ) { 
        the_post_thumbnail();
      } else {
        $image =  array( get_field('post_default', 'options'), "second");?>
        <img src="<?php echo $image[0]; ?>" alt="<?php echo $image['alt']; ?>" />
      <?php } ?>
    </div>
    <div class="post-contents">
      <h2 class="post-title"><?php the_title(); ?></h2>
      <p><?php echo get_excerpt(); ?></p>
      <a href="<?php the_permalink();?>" class="secondary-button">Read More</a>
    </div>
    <a class="link-cover" href="<?php the_permalink();?>"></a>
  </article>
<?php } ?>