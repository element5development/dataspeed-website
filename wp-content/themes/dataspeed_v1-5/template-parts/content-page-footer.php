<?php /*
FOOTER CONTENT
*/ ?>

<?php if (  is_page_template( 'template-styleguide.php' ) ) { ?>
  <footer id="footer" class="dark-bg">
    <div class="max-width clearfix">
      <div id="copyright">©Copyright <?php echo date('Y'); ?> Dataspeed, Inc.</div> 
      <a href="https://element5digital.com/" target="_blank" title="Element5 provides content and search marketing, web development and brand strategy." id="created-e5">
        Site by <img src="<?php bloginfo('stylesheet_directory'); ?>/img/e5-footer-grey.svg" alt="Eleemnt5 Crafting a better web to help brands thrive in a digital world." height="45">
      </a>
    </div>
  </footer>
<?php } else { ?>
  <footer id="footer" class="dark-bg">
    <div class="full-width clearfix">
      <div class="clearfix">
        <a href="/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/dataspeed-logo.png" alt="Dataspeed" /></a>
        <div id="copyright">©Copyright <?php echo date('Y'); ?> Dataspeed, Inc. <a href="<?php echo get_page_link(380); ?>">Privacy Policy</a></div> 
        <a href="https://element5digital.com/" target="_blank" title="Element5 provides content and search marketing, web development and brand strategy." id="created-e5">
          Site by <img src="<?php bloginfo('stylesheet_directory'); ?>/img/e5-footer-grey.svg" alt="Eleemnt5 Crafting a better web to help brands thrive in a digital world." height="45">
        </a>
      </div>
      <div class="clearfix">
        <a target="_blank" href="https://www.facebook.com/dataspeedinc/" class="one-fifth icon-link menu-facebook"></a>
        <a target="_blank" href="https://www.instagram.com/dataspeed_inc/" class="one-fifth icon-link menu-instagram"></a>
        <a target="_blank" href="https://www.linkedin.com/company/dataspeed-inc-" class="one-fifth icon-link menu-linkedin"></a>
        <a target="_blank" href="https://www.youtube.com/channel/UCquKa3e4RNM7SRaV8o14WFQ" class="one-fifth icon-link menu-youtube"></a>
        <a target="_blank" href="https://www.researchgate.net/profile/Paul_Fleck" class="one-fifth icon-link menu-rg"></a>
      </div>
    </div>
  </footer>
<?php } ?>