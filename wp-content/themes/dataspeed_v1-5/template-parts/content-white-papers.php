<?php /*
DISPLAY WHITE PAPER DOWNLOAD LINKS
*/ ?>

<section class="white-papers-container full-width">
  <div class="max-width">
    <div class="white-papers clearfix">
      <h2>Download our free whitepapers!</h2>
      <p>See why Dataspeed is leading the pack for Mobile Robotics and Autonomous Driving</p>
      <div class="one-half">
        <?php if( have_rows('robotics', 'options') ) {
          while ( have_rows('robotics', 'options') ) : the_row(); ?>
            <a href="javascript:void(0)" class="modal-trigger"><?php the_sub_field('title'); ?></a>
          <?php endwhile;
        } else {
          // no rows found
        } ?>
      </div>
      <div class="one-half">
        <?php if( have_rows('sutonomous', 'options') ) {
          while ( have_rows('sutonomous', 'options') ) : the_row(); ?>
            <a href="javascript:void(0)" class="modal-trigger"><?php the_sub_field('title'); ?></a>
          <?php endwhile;
        } else {
          // no rows found
        } ?>
      </div>
      <div class="white-paper-icon"></div>
    </div>
  </div>
</section>