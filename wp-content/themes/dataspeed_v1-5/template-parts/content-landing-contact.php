<?php /*
DISPLAY CONTACT FORM, CONTACT INFRO AND GOOGLE MAP
*/ ?>

  <section class="investors-contact full-width clearfix">
    <div class="one-half dark-bg">
      <div class="max-width">
        <h3>We’d love to work with you</h3>
        <?php echo do_shortcode('[gravityform id="6" title="false" description="false"]'); ?>
      </div>
    </div>
    <div class="one-half">
      <div class="max-width">
        <?php the_content(); ?>
      </div>
      <div class="image-bottom">
        <div class="one-half" style="background-image: url(http://dataspeedinc.com/wp-content/uploads/2016/11/baxter-base.jpg);">
          <p>Mobility Base</p>
        </div>
        <div class="one-half" style="background-image: url(http://dataspeedinc.com/wp-content/uploads/2016/11/vehicle-power-distribution.jpg)">
          <p>ADAS Kit</p>
        </div>
      </div>
    </div>
  </section>