<?php /*
DISPLAY SERVICE IMAGE & SUMMARY
*/ ?>

  <section class="service-summaries clearfix">
    <?php if( have_rows('service') ) { $i = 0;
      while ( have_rows('service') ) : $i++; the_row(); ?>

          <?php $image = get_sub_field('service_image'); ?>

          <?php if ( $i % 4 == 0 || $i == 1 ) { ?>
            <div class="service-section clearfix">
              <div class="one-half sticky-service service-image" style="background-image: url(<?php echo $image['url']; ?>);"></div>
              <div class="one-half service-content-container dark-bg">
                <div class="service-content">
                  <?php $image = get_sub_field('service_icon'); ?>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                  <h2><?php the_sub_field('service_title'); ?></h2>
                  <p class="service-header">WHAT WE DO</p>
                  <p><?php the_sub_field('what_we_do'); ?></p>
                  <p class="service-header">HOW WE DO IT</p>
                  <p><?php the_sub_field('how_we_do_it'); ?></p>
                </div>
              </div>
            </div>
          <?php } elseif ( $i % 3 == 0 ) { ?>
              <div class="service-section clearfix">
                  <div class="service-content dark-bg">
                    <?php $image = get_sub_field('service_icon'); ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <h2><?php the_sub_field('service_title'); ?></h2>
                    <p class="service-header">WHAT WE DO</p>
                    <p><?php the_sub_field('what_we_do'); ?></p>
                    <p class="service-header">HOW WE DO IT</p>
                    <p><?php the_sub_field('how_we_do_it'); ?></p>
                  </div>
              </div>
            </div> <!--CLOSE DUAL SERVICE CONTAINER-->
          <?php } else { ?>
            <div class="dual-service-section clearfix"> <!-- OPEN DUAL SERVICE CONTAINER -->
              <div class="service-section clearfix">
                  <div class="service-content right dark-bg">
                    <?php $image = get_sub_field('service_icon'); ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <h2><?php the_sub_field('service_title'); ?></h2>
                    <p class="service-header">WHAT WE DO</p>
                    <p><?php the_sub_field('what_we_do'); ?></p>
                    <p class="service-header">HOW WE DO IT</p>
                    <p><?php the_sub_field('how_we_do_it'); ?></p>
                  </div>
              </div>
          <?php } ?>
 
      <?php endwhile;
    } else {
      // no rows found
    } ?>
  </section>
