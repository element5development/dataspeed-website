<?php /*
MAIN NAVIGATION
*/ ?>

<?php if ( is_page_template( 'template-styleguide.php' ) ) { ?>
  <header id="sticky-nav" class="main-nav full-width dark-bg">
    <div class="max-width clearfix">
      <a href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/dataspeed-logo.png" alt="Dataspeed" /></a>
      <h2>Styleguide</h2>
    </div>
  </header>
<?php } else { ?>
  <header id="sticky-nav" class="main-nav full-width clearfix">
      <div class="one-half clearfix">
        <div class="header-left-container">
          <div class="header-left">
            <a href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/dataspeed-logo.png" alt="Dataspeed" /></a>
            <div class="main-menu-icon"></div>
          </div>
          <div style="clear: both"></div>
          <div class="main-menu clearfix">
            <!--SEARCH BAR-->
            <div class="search-bar">
              <?php get_search_form(); ?>
            </div>
            <div class="clearfix">
              <a href="tel:+1-<?php the_field('phone', 'options') ?>" class="one-third icon-link menu-phone"></a>
              <a target="_blank" href="https://www.google.com/maps/place/1935+Enterprise+Dr,+Rochester+Hills,+MI+48309/@42.6414991,-83.171129,17z/data=!3m1!4b1!4m5!3m4!1s0x8824c1f4da48dbb5:0x8478cf7668b2cdd4!8m2!3d42.6414991!4d-83.1689403" class="one-third icon-link menu-map"></a>
              <a class="one-third icon-link menu-search"></a>
            </div>
            <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
            <div class="clearfix">
              <a target="_blank" href="https://www.facebook.com/dataspeedinc/" class="one-fifth icon-link menu-facebook"></a>
              <a target="_blank" href="https://www.instagram.com/dataspeed_inc/" class="one-fifth icon-link menu-instagram"></a>
              <a target="_blank" href="https://www.linkedin.com/company/dataspeed-inc-" class="one-fifth icon-link menu-linkedin"></a>
              <a target="_blank" href="https://www.youtube.com/channel/UCquKa3e4RNM7SRaV8o14WFQ" class="one-fifth icon-link menu-youtube"></a>
              <a target="_blank" href="https://www.researchgate.net/profile/Paul_Fleck" class="one-fifth icon-link menu-rg"></a>
            </div>
          </div>
        </div>
      </div>
      <div class="one-half clearfix">
        <div class="header-right-container">
          <div class="top-bar-container clearfix">
            <div class="top-bar">
              <a href="tel:+1-<?php the_field('phone', 'options') ?>" class="header-phone icon-link"></a>
              <a href="https://www.facebook.com/dataspeedinc/" target="_blank" class="header-facebook icon-link"></a>
              <a href="https://www.instagram.com/dataspeed_inc/" target="_blank" class="header-instagram icon-link"></a>
              <a href="https://www.linkedin.com/company/dataspeed-inc-" target="_blank" class="header-linkedin icon-link"></a>
              <a href="https://www.youtube.com/channel/UCquKa3e4RNM7SRaV8o14WFQ" target="_blank" class="header-youtube icon-link"></a>
              <a href="https://www.researchgate.net/profile/Paul_Fleck" target="_blank" class="header-rg icon-link"></a>
              <a href="<?php echo get_page_link(357); ?>" class="header-careers">Careers</a> |
              <a href="<?php echo get_page_link(358); ?>" class="header-resources">Resources</a>
            </div>
          </div>
          <div class="header-right">
            <div class="header-tagline">We bring you the future you were always promised.</div>
          </div>
        </div>
      </div>
  </header>
<?php } ?>