<?php /*
SEARCH RESULT TEMPLATE
*/ ?>

  <?php
    //GET MAIN CATEGORY
    $categories = get_the_category();
  ?>
  <article class="post-preview search-result one-half dark-bg">
    <div class="post-top-half" style="background-image: url('<?php the_post_thumbnail_url()?>')">
      <div class="search-result-image" style="background-image: url('<?php echo $image[0]; ?>');"></div>
    </div>
    <div class="post-contents">
      <h2 class="post-title"><?php the_title(); ?></h2>
      <div class="search-result-content"><?php the_excerpt(); ?></div>
      <a href="<?php the_permalink();?>" class="secondary-button">Read More</a>
    </div>
    <a class="link-cover" href="<?php the_permalink();?>"></a>
  </article>