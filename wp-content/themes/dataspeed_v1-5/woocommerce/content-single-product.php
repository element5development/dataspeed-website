<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div class="full-width max-width">
    <div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php
            /**
             * woocommerce_before_single_product_summary hook.
             *
             * @hooked woocommerce_show_product_sale_flash - 10
             * @hooked woocommerce_show_product_images - 20
             */
            do_action( 'woocommerce_before_single_product_summary' );
        ?>

        <div class="summary entry-summary">
        <?php 
            woocommerce_template_single_title();
            woocommerce_template_single_rating();
            woocommerce_template_single_price();
            woocommerce_template_single_excerpt();
            ?>
            <?php if ( has_term( 'vehicle-maintenance', 'product_cat' ) ) { ?>
            <?php //to add another to the list (product_cat is the tag woocommerce uses for product categories): if ( has_term( array('vehicle-maintenance','another-cat'), 'product_cat' ) ) { ?>
            <?php //add a product ID: if (is_single(theproductid)) { ?>
            <?php //combine them to form Voltron as an 'or' conditional: If(is_single(theproductid) || has_term ('vehicle_maintenance','product_cat') { ?>
            <h3>VIN Numbers</h3>
            <p>Select the vin numbers you would like to update.</p>
            <p><a href="../../my-account/edit-account/">Go to My Account to enter your VIN numbers</a></p>
            <div>
                <?php
                $user_id = get_current_user_id();
                $user = get_userdata( $user_id );

                if ( !$user )
                return;

                $vin_numbers = get_user_meta( $user_id, 'vin_numbers',true);
                if( !$vin_numbers) { ?>
                <!--<a href="../../my-account/edit-account/">Go to My Account to enter your VIN numbers</a>-->

                <?php } else {
                foreach( $vin_numbers as $vin_number) { ?>
                <input type="checkbox" name="vin" value="<?php echo $vin_number['vin_number'];?>"><?php echo $vin_number['vin_number']; ?><br>
                <?php } }?>
                <br>
            </div>
            <div class="required_message"><p>Please select a VIN number to add to your order</p></div>
            <?php } ?>
            <?php
            woocommerce_template_single_add_to_cart();
            woocommerce_template_single_meta();
            woocommerce_template_single_sharing();
            
            ?>
            
        </div><!-- .summary -->

        <?php
            /**
             * woocommerce_after_single_product_summary hook.
             *
             * @hooked woocommerce_output_product_data_tabs - 10
             * @hooked woocommerce_upsell_display - 15
             * @hooked woocommerce_output_related_products - 20
             */
            do_action( 'woocommerce_after_single_product_summary' );
        ?>

        <meta itemprop="url" content="<?php the_permalink(); ?>" />

    </div><!-- #product-<?php the_ID(); ?> -->
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>
