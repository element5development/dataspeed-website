<?php

//SETUP WORDPRESS & BACKEND
  // REMOVE PARENT WIDGET AREAS
    function remove_parent_theme_widgets(){
      unregister_sidebar( 'sidebar-1' );
      unregister_sidebar( 'sidebar-2' );
      unregister_sidebar( 'sidebar-3' );
    }
    add_action( 'widgets_init', 'remove_parent_theme_widgets', 11 );
  // REMOVE PARENT THEME MENUS
    function remove_default_menu(){
      unregister_nav_menu('primary');
      unregister_nav_menu('social');
    }
    add_action( 'after_setup_theme', 'remove_default_menu', 11 );
  // ENABLE JQUERY TO WORDPRESS
      if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
      function my_jquery_enqueue() {
         wp_deregister_script('jquery');
         wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js", false, null);
         wp_enqueue_script('jquery');
      }
  // ENABLE SHORTCODES IN WIDGETS
    add_filter('widget_text', 'do_shortcode');
  // ENABLE ACF OPTIONS PAGE
    if( function_exists('acf_add_options_page') ) {
      acf_add_options_page();
      acf_add_options_sub_page('Theme-Options');
      acf_add_options_sub_page('Styleguide');
    }
  // ALLOW SVG & ZIP FILES IN WORDPRESS
    function cc_mime_types($mimes) {
      $mimes['svg'] = 'image/svg+xml';
      $mimes['zip'] = 'application/zip';
      return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');

// Register Custom Post Type Team Member
// Post Type Key: teammember
function create_teammember_cpt() {

  $labels = array(
    'name' => __( 'Team Members', 'Post Type General Name', 'textdomain' ),
    'singular_name' => __( 'Team Member', 'Post Type Singular Name', 'textdomain' ),
    'menu_name' => __( 'Team Members', 'textdomain' ),
    'name_admin_bar' => __( 'Team Member', 'textdomain' ),
    'archives' => __( 'Team Member Archives', 'textdomain' ),
    'attributes' => __( 'Team Member Attributes', 'textdomain' ),
    'parent_item_colon' => __( 'Parent Team Member:', 'textdomain' ),
    'all_items' => __( 'All Team Members', 'textdomain' ),
    'add_new_item' => __( 'Add New Team Member', 'textdomain' ),
    'add_new' => __( 'Add New', 'textdomain' ),
    'new_item' => __( 'New Team Member', 'textdomain' ),
    'edit_item' => __( 'Edit Team Member', 'textdomain' ),
    'update_item' => __( 'Update Team Member', 'textdomain' ),
    'view_item' => __( 'View Team Member', 'textdomain' ),
    'view_items' => __( 'View Team Members', 'textdomain' ),
    'search_items' => __( 'Search Team Member', 'textdomain' ),
    'not_found' => __( 'Not found', 'textdomain' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
    'featured_image' => __( 'Featured Image', 'textdomain' ),
    'set_featured_image' => __( 'Set featured image', 'textdomain' ),
    'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
    'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
    'insert_into_item' => __( 'Insert into Team Member', 'textdomain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this Team Member', 'textdomain' ),
    'items_list' => __( 'Team Members list', 'textdomain' ),
    'items_list_navigation' => __( 'Team Members list navigation', 'textdomain' ),
    'filter_items_list' => __( 'Filter Team Members list', 'textdomain' ),
  );
  $args = array(
    'label' => __( 'Team Member', 'textdomain' ),
    'description' => __( '', 'textdomain' ),
    'labels' => $labels,
    'menu_icon' => 'dashicons-admin-users',
    'supports' => array('title', 'thumbnail', 'author', 'page-attributes', 'custom-fields', ),
    'public' => true,
    'taxonomies'  => array( 'category' ),
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_admin_bar' => false,
    'show_in_nav_menus' => false,
    'can_export' => true,
    'has_archive' => false,
    'hierarchical' => false,
    'exclude_from_search' => false,
    'show_in_rest' => true,
    'publicly_queryable' => false,
    'capability_type' => 'post',
  );
  register_post_type( 'teammember', $args );

}
add_action( 'init', 'create_teammember_cpt', 0 );

// Register Custom Post Type Story
// Post Type Key: story
function create_story_cpt() {

  $labels = array(
    'name' => __( 'Customer Stories', 'Post Type General Name', 'textdomain' ),
    'singular_name' => __( 'Story', 'Post Type Singular Name', 'textdomain' ),
    'menu_name' => __( 'Customer Stories', 'textdomain' ),
    'name_admin_bar' => __( 'Story', 'textdomain' ),
    'archives' => __( 'Story Archives', 'textdomain' ),
    'attributes' => __( 'Story Attributes', 'textdomain' ),
    'parent_item_colon' => __( 'Parent Story:', 'textdomain' ),
    'all_items' => __( 'All Customer Stories', 'textdomain' ),
    'add_new_item' => __( 'Add New Story', 'textdomain' ),
    'add_new' => __( 'Add New', 'textdomain' ),
    'new_item' => __( 'New Story', 'textdomain' ),
    'edit_item' => __( 'Edit Story', 'textdomain' ),
    'update_item' => __( 'Update Story', 'textdomain' ),
    'view_item' => __( 'View Story', 'textdomain' ),
    'view_items' => __( 'View Customer Stories', 'textdomain' ),
    'search_items' => __( 'Search Story', 'textdomain' ),
    'not_found' => __( 'Not found', 'textdomain' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
    'featured_image' => __( 'Featured Image', 'textdomain' ),
    'set_featured_image' => __( 'Set featured image', 'textdomain' ),
    'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
    'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
    'insert_into_item' => __( 'Insert into Story', 'textdomain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this Story', 'textdomain' ),
    'items_list' => __( 'Customer Stories list', 'textdomain' ),
    'items_list_navigation' => __( 'Customer Stories list navigation', 'textdomain' ),
    'filter_items_list' => __( 'Filter Customer Stories list', 'textdomain' ),
  );
  $args = array(
    'label' => __( 'Story', 'textdomain' ),
    'description' => __( '', 'textdomain' ),
    'labels' => $labels,
    'menu_icon' => 'dashicons-edit',
    'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'post-formats', 'custom-fields', ),
    'taxonomies' => array('category'),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_admin_bar' => false,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'hierarchical' => true,
    'exclude_from_search' => false,
    'show_in_rest' => true,
    'publicly_queryable' => true,
    'capability_type' => 'post',
  );
  register_post_type( 'story', $args );

}
add_action( 'init', 'create_story_cpt', 0 );


  // ALLOW EDITOR ACCESS TO GRAVITY FORMS, MENUS & WIDGETS
    function add_grav_forms(){
      $role = get_role('editor');
      $role->add_cap('gform_full_access');
      $role->add_cap('edit_theme_options');
      $role->add_cap('edit_users');
      $role->add_cap('list_users');
      $role->add_cap('promote_users');
      $role->add_cap('create_users');
      $role->add_cap('add_users');
      $role->add_cap('delete_users');
    }
    add_action('admin_init','add_grav_forms');
  // ADD SMOOTHSCROLL CLASS TO MENU LINKS WITH REL
      function add_menuclass($ulclass) {
        return preg_replace('/<a rel="smoothScroll"/', '<a rel="smoothScroll" class="smoothScroll"', $ulclass);
      }
      add_filter('wp_nav_menu','add_menuclass');
  //LIMIT EXCERPT LENGTH
      function get_excerpt(){
      $excerpt = get_the_content();
      $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
      $excerpt = strip_shortcodes($excerpt);
      $excerpt = strip_tags($excerpt);
      $excerpt = substr($excerpt, 0, 300);
      $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
      $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
      $excerpt = $excerpt.'...';
      return $excerpt;
      }
  // STYLEGUIDE HEX TO RGB
    function hex2rgb( $colour ) {
      if ( $colour[0] == '#' ) { $colour = substr( $colour, 1 ); }
      if ( strlen( $colour ) == 6 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
      } elseif ( strlen( $colour ) == 3 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
      } else {
        return false;
      }
      $r = hexdec( $r ); $g = hexdec( $g ); $b = hexdec( $b );
      return 'rgb('.$r.', '.$g.', '.$b.')';
    }
  // STYLEGUIDE RGB TO CMYK
    function hex2rgb2($hex) {
      $color = str_replace('#','',$hex);
      $rgb = array(
        'r' => hexdec(substr($color,0,2)),
        'g' => hexdec(substr($color,2,2)),
        'b' => hexdec(substr($color,4,2)),
      );
      return $rgb;
    }
    function rgb2cmyk($var1,$g=0,$b=0) {
      if (is_array($var1)) { $r = $var1['r']; $g = $var1['g']; $b = $var1['b'];
      } else { $r = $var1; }
      $r = $r / 255; $g = $g / 255; $b = $b / 255;
      $bl = 1 - max(array($r,$g,$b));
      $c = ( 1 - $r - $bl ) / ( 1 - $bl ); $m = ( 1 - $g - $bl ) / ( 1 - $bl ); $y = ( 1 - $b - $bl ) / ( 1 - $bl );
      $c = round($c * 100); $m = round($m * 100); $y = round($y * 100); $bl = round($bl * 100);
      return 'cmyk('.$c.', '.$m.', '.$y.', '.$bl.')';
    }
//SETUP WORDPRESS & BACKEND


//CUSTOMIZE CONTENT FOR CLIENT
  // ADD MENUS FOR NAVIGATION
    function register_menu () {
      register_nav_menu('main-menu', __('Main Menu'));
      register_nav_menu('post-category-nav', __('Post Categories'));
      register_nav_menu('resource-category-nav', __('Resource Categories'));
      register_nav_menu('customer-story-nav', __('Customer Stories Categories'));
    }
    add_action('init', 'register_menu');
  // PRIMARY BUTTON SHORTCODE
    function primary_button($atts, $content = null) {
      extract( shortcode_atts( array(
        'target' => '',
        'url' => '#',
        'arrow' => ''
      ), $atts ) );
      return '<a target="'.$target.'" href="'.$url.'" class="primary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
    }
    add_shortcode('primary-btn', 'primary_button');
  // SECONDARY BUTTON SHORTCODE
    function secondary_button($atts, $content = null) {
      extract( shortcode_atts( array(
        'target' => '',
        'url' => '#',
        'arrow' => ''
      ), $atts ) );
      return '<a target="'.$target.'" href="'.$url.'" class="secondary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
    }
    add_shortcode('secondary-btn', 'secondary_button');
  // LEAGAL TEXT SHORTCODE
    function legal_text($atts, $content = null) {
      extract( shortcode_atts( array( ), $atts ) );
      return '<p class="legal-text">' . do_shortcode($content) . '</p>';
    }
    add_shortcode('legal-text', 'legal_text');
  //ADD CUSTOM POST TYPE
    function create_posttype() {
      // Case Studies
      $labels = array(
          'name'                => _x( 'Case Studies', 'Post Type General Name', 'twentysixteen' ),
          'singular_name'       => _x( 'Case Study', 'Post Type Singular Name', 'twentysixteen' ),
      );
      $args = array(
          'label'               => __( 'Case Studies', 'twentysixteen' ),
          'labels'              => $labels,
          'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
          'taxonomies'          => array( 'categories' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => true,
          'capability_type'     => 'page',
      );
      register_post_type( 'case-studies', $args );
      // Resource
      $labels = array(
          'name'                => _x( 'Resources', 'Post Type General Name', 'twentysixteen' ),
          'singular_name'       => _x( 'Resource', 'Post Type Singular Name', 'twentysixteen' ),
      );
      $args = array(
          'label'               => __( 'Resources', 'twentysixteen' ),
          'labels'              => $labels,
          'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
          'taxonomies'          => array( 'category' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => true,
          'capability_type'     => 'page',
      );
      register_post_type( 'Resources', $args );
      // Customers
      $labels = array(
          'name'                => _x( 'Clients', 'Post Type General Name', 'twentysixteen' ),
          'singular_name'       => _x( 'Client', 'Post Type Singular Name', 'twentysixteen' ),
      );
      $args = array(
          'label'               => __( 'Clients', 'twentysixteen' ),
          'labels'              => $labels,
          'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
          'taxonomies'          => array( 'categories' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'can_export'          => true,
          'has_archive'         => false,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => true,
          'capability_type'     => 'page',
      );
      register_post_type( 'Clients', $args );
      // Careers
      $labels = array(
          'name'                => _x( 'Career', 'Post Type General Name', 'twentysixteen' ),
          'singular_name'       => _x( 'Career', 'Post Type Singular Name', 'twentysixteen' ),
      );
      $args = array(
          'label'               => __( 'Career', 'twentysixteen' ),
          'labels'              => $labels,
          'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
          'taxonomies'          => array( 'categories' ),
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => true,
          'publicly_queryable'  => true,
          'exclude_from_search' => true,
          'capability_type'     => 'page',
      );
      register_post_type( 'Career', $args );
    }
    add_action( 'init', 'create_posttype' );

//CUSTOMIZE CONTENT FOR CLIENT


//REDIRECT WOOCOMMERCE IF NOT LOGGED IN (Customer Portal)

function customer_portal_redirect() {
    if (
        ! is_user_logged_in()
        && (is_woocommerce() || is_cart() || is_checkout())
    ) {
        // feel free to customize the following line to suit your needs
        wp_redirect(site_url('my-account/'));
        exit;
    }
}
add_action('template_redirect', 'customer_portal_redirect');

//REDIRECT WOOCOMMERCE IF NOT LOGGED IN (Customer Portal)

//REMOVE BREADCRUMBS
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
//REMOVE BREADCRUMBS

//REMOVE PRODUCT DESCRIPTION TITLE ON SINGLE PRODUCTS
add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
return '';
}
//REMOVE PRODUCT DESCRIPTION TITLE ON SINGLE PRODUCTS

// Remove WooCommerce Updater
remove_action('admin_notices', 'woothemes_updater_notice');
// Remove WooCommerce Updater

//Add fields to User My Account

//Load license functions
require_once('dataspeed_license.php');
//Load repeater js
add_action('wp_enqueue_scripts','repeater_js');
function repeater_js() {
  wp_enqueue_script('repeater', get_stylesheet_directory_uri() . '/js/jquery.repeater.min.js', array('jquery'), '', true);
  wp_enqueue_script('vin_cart', get_stylesheet_directory_uri(). '/js/vin-cart-controls.js', array('jquery'),'',true);
}
add_action( 'woocommerce_edit_account_form', 'my_woocommerce_edit_account_form' );
 
function my_woocommerce_edit_account_form() {
 
  $user_id = get_current_user_id();
  $user = get_userdata( $user_id );
 
  if ( !$user )
    return;
 
  $vin_numbers = get_user_meta( $user_id, 'vin_numbers',true);
 
  
?>

<fieldset>
<legend>VIN Numbers</legend>
<div class="vin_number">
  <div data-repeater-list="group-a">
    <?php if(!$vin_numbers){ ?>
    <div data-repeater-item>
        <input type="text" name="vin_number" placeholder="##" value="<?php echo esc_attr( $vin_number['vin_number'] ); ?>"/>
        <input data-repeater-delete type="button" value="Delete"/>
      </div>
    <?php } else { 

      foreach($vin_numbers as $vin_number){ ?>
      <div data-repeater-item>
        <input type="text" name="vin_number" placeholder="##" value="<?php echo esc_attr( $vin_number['vin_number'] ); ?>"/>
        <input data-repeater-delete type="button" value="Delete"/>
      </div>
    <?php  } }?>
    </div>
    <input data-repeater-create type="button" value="Add"/>
  </fieldset>
 
<?php
 
} 

add_action( 'woocommerce_save_account_details', 'my_woocommerce_save_account_details' );
 
function my_woocommerce_save_account_details( $user_id ) {

  update_user_meta( $user_id, 'vin_numbers', $_POST['group-a'] );


}

add_filter('woocommerce_add_cart_item_data', 'vin_add_item_data', 10, 2);
add_filter('woocommerce_get_cart_item_from_session','vin_wc_get_cart_item_from_session',10,2);
add_filter('woocommerce_get_item_data', 'vin_wc_get_item_data', 10, 2);
add_action('woocommerce_add_order_item_meta', 'vin_wc_order_item_meta', 10, 2);

function vin_add_item_data($cart_item_meta, $product_id) {
  if(isset($_POST['vin_number'])) {
      foreach ($_POST['vin_number'] as $key=>$value){
        $license_key = dsLicenseGenerateAdasKit($value, "2017/12/01");
          //Need to add the expiration date from the membership plan.
        $cart_item_meta['vin_number_'.$key.''] = $value;
        $cart_item_meta['license_key_'.$key.''] = $license_key;
      }
    }
  

  return $cart_item_meta;
}

function vin_wc_get_cart_item_from_session($cart_item, $values) {
  foreach($values as $key => $value){
   
      $cart_item[$key] = $values[$key];
  }

  return $cart_item;
}

function vin_wc_get_item_data($other_data, $cart_item) {

    foreach($cart_item as $key=>$value){
      if(substr($key, 0, 11) === 'vin_number_'){
        $vin_num = intval(substr($key, 11, 1)) + 1;
        $data = $cart_item[$key];
        $other_data[] = array('name' => 'VIN Number '.$vin_num, 'value' => $data);
      }

      if(substr($key, 0, 12) === 'license_key_'){
        $lic_num = intval(substr($key, 12, 1)) + 1;
        $data = $cart_item[$key];
        $other_data[] = array('name' => 'License Key '.$lic_num, 'value' => $data);
      }
      
    }
    return $other_data;
}

function vin_wc_order_item_meta($item_id, $cart_item) {


  foreach($cart_item as $key=>$value){
    if(substr($key, 0, 11) === 'vin_number_'){
      $vin_num = intval(substr($key, 11, 1)) + 1;
      $data = $cart_item[$key];
      woocommerce_add_order_item_meta($item_id, 'VIN Number '.$vin_num, $data);
    }

    if(substr($key, 0, 12) === 'license_key_'){
      $lic_num = intval(substr($key, 12, 1)) + 1;
      $data = $cart_item[$key];
      woocommerce_add_order_item_meta($item_id, 'License Key '.$lic_num, $data);
    }
    
  }

}


//Versions make me cranky
function dataspeed_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'dataspeed_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'dataspeed_remove_wp_ver_css_js', 9999 );

?>