<?php /*
Template Name: Home Page
*/ ?>

<?php get_header(); ?>

<main class="full-width">

    <?php get_template_part( 'template-parts/content', 'page-top' ); //PAGE TITLE, FEATURED IMAGE, BREADCRUMBS ?>
    <?php get_template_part( 'template-parts/content', 'robot-preview' ); //DISPLAY ROBOT VIDEOS WITH SPECIAL SCROLLING EFFECT ?>

    <div class="slide-effect-2">
      <?php get_template_part( 'template-parts/content', 'insperation' ); //INSPERATION QUOTE ?> 
      <?php get_template_part( 'template-parts/content', 'driverless-preview' ); //DRIVERLESS CARS VIDEOS WITH SPECIAL SCROLLING EFFECT ?>
    </div>

    <div class="slide-effect-3">
      <?php get_template_part( 'template-parts/content', 'make-preview' ); //PRODUCT PREVIEWS ?> 
      <?php get_template_part( 'template-parts/content', 'studies-preview' ); //LATEST CASE STUDIES WITH SPECIAL SCROLLING EFFECT ?>
    </div>

    <div class="slide-effect-4">
      <?php get_template_part( 'template-parts/content', 'logo-slider' ); //SUPPORTERS LOGO SLIDER ?>
      <?php get_template_part( 'template-parts/content', 'news-preview' ); //LATEST POSTS WITH SPECIAL SCROLLING EFFECT ?>
    </div>

    <?php get_template_part( 'template-parts/content', 'contact-preview' ); //CONTACT FORM, CONTACT INFRO AND GOOGLE MAP ?>

</main>

<?php get_footer(); ?>