<?php /*
Template Name: Whitepaper thanks
*/ ?>

<?php get_header(); ?>

  <main class="full-width">


    <!-- ADD PAGE CONTENT -->
    <?php if(!empty( get_the_content() ) ) { ?>
      <div class="page-contents max-width" style="min-height: 81vh">
        <?php the_content(); ?>
      </div>
    <?php } ?>


  </main>

<?php get_footer(); ?>