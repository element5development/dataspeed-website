/* ENTRANCE ANIMATIONS */

jQuery(document).ready(function() {
  //MENU OPEN & CLOSE
  jQuery('.main-menu-icon').click( function() {
    if ( jQuery('.main-menu-icon').hasClass('open') ) {
      jQuery('.main-menu').removeClass('open');
      jQuery('.main-menu-icon').removeClass('open');
    } else {
      jQuery('.main-menu').addClass('open');
      jQuery('.main-menu-icon').addClass('open');
    }
  });
  //SEARCH BAR OPEN AND CLOSE
  jQuery('.menu-search').click( function() {
    jQuery('.search-bar').addClass('activated');
  });
  jQuery('.search-bar-icon').click( function() {
    jQuery('.search-bar').removeClass('activated');
  });
  //CASE STUDY SELECTION
  jQuery('#study-small-1').click( function() {
    jQuery('.study-small-preview').removeClass('selected');
    jQuery('#study-small-1').addClass('selected');
    jQuery('.studies-preview .content-right .sticky-slide-content').addClass('fadeOut hidden');
    jQuery('.studies-preview .content-right .sticky-slide-content').removeClass('selected fadeIn');
    jQuery('#study-image-1').removeClass('fadeOut hidden');
    jQuery('#study-image-1').addClass('selected fadeIn');
    jQuery('.studies-preview-content').addClass('fadeOutLeft hidden');
    jQuery('.studies-preview-content').removeClass('selected fadeInRight');
    jQuery('#study-preview-1').removeClass('fadeOutLeft hidden');
    jQuery('#study-preview-1').addClass('selected fadeInRight');
  });
  jQuery('#study-small-2').click( function() {
    jQuery('.study-small-preview').removeClass('selected');
    jQuery('#study-small-2').addClass('selected');
    jQuery('.studies-preview .content-right .sticky-slide-content').addClass('fadeOut hidden');
    jQuery('.studies-preview .content-right .sticky-slide-content').removeClass('selected fadeIn');
    jQuery('#study-image-2').removeClass('fadeOut hidden');
    jQuery('#study-image-2').addClass('selected fadeIn');
    jQuery('.studies-preview-content').addClass('fadeOutLeft hidden');
    jQuery('.studies-preview-content').removeClass('selected fadeInRight');
    jQuery('#study-preview-2').removeClass('fadeOutLeft hidden');
    jQuery('#study-preview-2').addClass('selected fadeInRight');
  });
  jQuery('#study-small-3').click( function() {
    jQuery('.study-small-preview').removeClass('selected');
    jQuery('#study-small-3').addClass('selected');
    jQuery('.studies-preview .content-right .sticky-slide-content').addClass('fadeOut hidden');
    jQuery('.studies-preview .content-right .sticky-slide-content').removeClass('selected fadeIn');
    jQuery('#study-image-3').removeClass('fadeOut hidden');
    jQuery('#study-image-3').addClass('selected fadeIn');
    jQuery('.studies-preview-content').addClass('fadeOutLeft hidden');
    jQuery('.studies-preview-content').removeClass('selected fadeInRight');
    jQuery('#study-preview-3').removeClass('fadeOutLeft hidden');
    jQuery('#study-preview-3').addClass('selected fadeInRight');
  });
  jQuery('#study-small-4').click( function() {
    jQuery('.study-small-preview').removeClass('selected');
    jQuery('#study-small-4').addClass('selected');
    jQuery('.studies-preview .content-right .sticky-slide-content').addClass('fadeOut hidden');
    jQuery('.studies-preview .content-right .sticky-slide-content').removeClass('selected fadeIn');
    jQuery('#study-image-4').removeClass('fadeOut hidden');
    jQuery('#study-image-4').addClass('selected fadeIn');
    jQuery('.studies-preview-content').addClass('fadeOutLeft hidden');
    jQuery('.studies-preview-content').removeClass('selected fadeInRight');
    jQuery('#study-preview-4').removeClass('fadeOutLeft hidden');
    jQuery('#study-preview-4').addClass('selected fadeInRight');
  });



});