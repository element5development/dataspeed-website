/* CHART AND GRAPH CREATION */
Chart.defaults.global.legend.display = false; //remove legend from charts
Chart.defaults.global.tooltips.enabled = false; //remove tooltip

//STYLE GUIDE CHARTS
var data = {
    labels: [ "grey", "green" ],
    datasets: [{
      data: [25, 75],
      backgroundColor: [ "#afbed0", "#20b05a", ],
      hoverBackgroundColor: [ "#afbed0", "#20b05a" ], 
      borderWidth: 0 
    }]
};
var options = { cutoutPercentage: 75 };
var ctx = document.getElementById("doughnut-chart-one");
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: options
});

var data = {
    labels: [ "grey", "green" ],
    datasets: [{
      data: [45, 55],
      backgroundColor: [ "#afbed0", "#20b05a", ],
      hoverBackgroundColor: [ "#afbed0", "#20b05a" ],
      borderWidth: 0 
    }]
};
var options = { cutoutPercentage: 75 };
var ctx = document.getElementById("doughnut-chart-two");
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: options
});

var data = {
    labels: [ "grey", "green" ],
    datasets: [{
      data: [10, 90],
      backgroundColor: [ "#afbed0", "#20b05a", ],
      hoverBackgroundColor: [ "#afbed0", "#20b05a" ],
      borderWidth: 0 
    }]
};
var options = { cutoutPercentage: 75 };
var ctx = document.getElementById("doughnut-chart-three");
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: options
});
//END STYLE GUIDE CHARTS