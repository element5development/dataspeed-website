(function($){


	$('.vin_number').repeater();

	$('.single_add_to_cart_button').addClass('disabled');

	$('input[name="vin"]').change(function(){
		if(this.checked) {
			$('.single_add_to_cart_button').removeClass('disabled');
			$('.required_message').fadeOut('slow');
			$('.cart').append('<input type="hidden" name="vin_number[]" value="'+$(this).val()+'">');
			
		} else {
			$('.single_add_to_cart_button').addClass('disabled');
			$('.required_message').fadeIn('slow');
			$('.cart input[value="'+$(this).val()+'"]').remove();
		}

		//var quantity = $('input[type="checkbox"]:checked').length;

		//$('input[name="quantity"]').val(quantity);
	});

})(jQuery);