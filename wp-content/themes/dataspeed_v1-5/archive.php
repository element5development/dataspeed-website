<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES FOR BLOG
*/ ?>

<?php get_header(); ?>

<main class="full-width">

  <!-- PAGE TITLE, FEATURED IMAGE, BREADCRUMBS -->
    <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- CATEGORY MENU -->
    <nav class="secondary-nav max-width">
      <h2>Sort by Category</h2>
      <?php wp_nav_menu( array( 'theme_location' => 'post-category-nav' ) ); ?>
    </nav>

  <!-- POST ARCHIEVE -->
    <?php  
      $year = get_query_var('year');
      $month = get_query_var('monthnum');
    ?>
    <section class="blog archieve-filter max-width clearfix">
      <?php echo do_shortcode('[ajax_load_more post_type="post" year=" ' . $year . ' " month=" ' . $month . ' " posts_per_page="6" scroll="false" transition_container="false" button_label="Load More"]'); ?>
    </section>

  <!-- SUPPORTERS LOGO SLIDER -->
    <?php get_template_part( 'template-parts/content', 'logo-slider' ); ?>

</main>

<?php get_footer(); ?>