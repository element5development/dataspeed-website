<?php /*
THE TEMPLATE FOR DISPLAYING SINGLE CUSTOMER STORY 
*/ ?>

<?php get_header(); ?>

  <main class="full-width">

    <!-- PAGE TITLE, FEATURED IMAGE, BREADCRUMBS -->
    <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

    <!-- POST CONTENT -->
    <article class="singe-post max-width">
      <?php if ( 'career' == get_post_type() ) { ?>
        <a href="/careers" class="back-to-news">Back to Careers</a>
        <a href="/careers/apply-now/?job-title=<?php the_title(); ?>" class="primary-button apply-btn">Apply Now</a>
        <div style="clear: both"></div>
      <?php } elseif ('story' == get_post_type() ) { ?>
        <a href="/story" class="back-to-news">Back to Customer Stories</a>
        <div style="clear: both"></div>
      <?php } else { ?>
        <a href="/news" class="back-to-news">Back to all articles</a>
        <div class="post-category"><?php the_category(); ?></div>
        <div style="clear: both"></div>
      <?php } ?>
      <h1><?php the_title(); ?></h1>
      <?php if ( 'story' == get_post_type() ) : //CAREER POST CONTENT ?>
        <div class="pub-data-contain">
          <?php if( get_field('publication_name') ): ?>
            <span class="pub-data">Publication: </span><a class="pub-data" href="<?php the_field('publication_url')?>"><?php the_field('publication_name')?></a><?php if( get_field('publication_date') ): ?> |<?php endif; ?>
          <?php endif; ?>
          <?php if( get_field('publication_date') ): ?>
            <span class="pub-data"><?php the_field('publication_date')?> <?php if( get_field('publication_author') ): ?> |<?php endif; ?></span>
          <?php endif; ?>
          <?php if( get_field('publication_author') ): ?>
            <span class="pub-data">Author: <?php the_field('publication_author')?></span>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <?php if ( 'career' == get_post_type() ) { //CAREER POST CONTENT ?>
        <div class="job-details ful-width clearfix">
          <?php the_field('employement_type'); ?><span>|</span><?php the_field('location'); ?><span>|</span><?php the_field('job_type'); ?></div>
        </div>
        <div class="job-opportunity ful-width clearfix">
          <h2>The Opportunity</h2>
          <p><?php the_field('the_opportunity'); ?></p>
        </div>
        <div class="job-infos ful-width clearfix">
          <div class="one-half">
            <?php if( have_rows('responsibilities') ) { ?>
              <h3>Responsibilities:</h3>
              <ul>
                <?php while ( have_rows('responsibilities') ) : the_row(); ?>
                  <li><?php the_sub_field('responsibility')?></li>
                <?php endwhile; ?>
              </ul>
            <?php } else {
              // no rows found
            } ?>
          </div>
          <div class="one-half">
            <?php if( have_rows('requirements') ) { ?>
              <h3>Requirements:</h3>
              <ul>
                <?php while ( have_rows('requirements') ) : the_row(); ?>
                  <li><?php the_sub_field('Requirment')?></li>
                <?php endwhile; ?>
              </ul>
            <?php } else {
              // no rows found
            } ?>
          </div>
        </div>
      <?php } ?>
      <?php the_content(); ?>
    </article>

    <!-- POST NAVOGATION -->
    <?php if ( 'career' == get_post_type() ) {
      //nothing
    } else { ?>
      <div class="post-nav full-width clearfix">
        <div class="one-half previous"><?php previous_post_link('%link','Previous Article'); ?></div>
        <div class="one-half next"><?php next_post_link('%link','Next Article'); ?></div>
      </div>
    <?php } ?>

    <!-- SUPPORTERS LOGO SLIDER -->
    <?php get_template_part( 'template-parts/content', 'logo-slider' ); ?>

  </main>

<?php get_footer(); ?>