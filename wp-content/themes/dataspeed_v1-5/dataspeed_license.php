<?php

/* Dataspeed Inc. license generation and validation
* Last updated 3/10/2017
* External Functions:
*   dsLicenseGenerateVpdBluetooth($serial)
*   dsLicenseGenerateAdasKit($vin, $date)
*/

function hexToString($input) {
    $output = "";
    for($i = 0; $i < sizeof($input); $i++) {
        $output .= sprintf("%02X", $input[$i]);
    }
    return strtoupper($output);
}

function generateHash($input) {
    $pearsonTable = array(
        // 0x00 to 0xff in any (random) order
        0xc2, 0xf4, 0x6c, 0x9c, 0xfd, 0x51, 0x33, 0x0a,
        0x0e, 0x40, 0x19, 0xed, 0x03, 0xac, 0x00, 0x10,
        0xf1, 0xec, 0x2c, 0x7b, 0x69, 0x6d, 0xbc, 0x9b,
        0x3f, 0x56, 0x85, 0x93, 0xc7, 0xca, 0x1c, 0xf0,
        0xe4, 0xd0, 0x47, 0x7e, 0x05, 0xe6, 0xb2, 0x1a,
        0x43, 0xf6, 0x6b, 0x26, 0xd4, 0x35, 0xb7, 0x45,
        0xbd, 0xfb, 0x0c, 0x55, 0x75, 0x1e, 0x92, 0x37,
        0x7c, 0x5a, 0xc5, 0xeb, 0xda, 0x96, 0xdf, 0x4f,
        0x25, 0x41, 0xcb, 0x42, 0x13, 0xfa, 0x46, 0xa4,
        0xe5, 0x94, 0xa9, 0x12, 0x24, 0x8b, 0x0d, 0xe9,
        0x90, 0x30, 0x16, 0x1f, 0x68, 0xb5, 0xa3, 0xd8,
        0x9d, 0xc8, 0xd2, 0x1d, 0x6e, 0x9e, 0xb8, 0xd9,
        0xb6, 0xcd, 0x31, 0xf5, 0x78, 0x62, 0x3e, 0x5b,
        0x49, 0x11, 0x38, 0xd7, 0x4a, 0xde, 0xce, 0xab,
        0x86, 0xc6, 0xb4, 0x48, 0x6f, 0x0b, 0x98, 0x57,
        0x80, 0x74, 0x06, 0x76, 0x9f, 0xdc, 0xd3, 0x52,
        0x66, 0x3d, 0x34, 0x21, 0x01, 0xc3, 0xe8, 0x4e,
        0x32, 0xa0, 0x6a, 0x02, 0xe3, 0xba, 0x4c, 0x18,
        0x71, 0xb1, 0x89, 0x5c, 0x5f, 0x59, 0x5e, 0xa1,
        0x44, 0x8d, 0xf2, 0xf9, 0x2e, 0x77, 0x67, 0x7d,
        0xa2, 0x27, 0x3c, 0x4b, 0x8e, 0xd6, 0xcf, 0x7f,
        0x07, 0x3b, 0x72, 0x2f, 0xef, 0x50, 0x0f, 0x64,
        0x20, 0xaf, 0x60, 0xf8, 0x8c, 0xc0, 0x53, 0xae,
        0x87, 0x7a, 0x9a, 0xc9, 0x95, 0x54, 0xea, 0x99,
        0x65, 0xaa, 0x2a, 0x82, 0xa6, 0xa5, 0x63, 0xfe,
        0xbf, 0xee, 0x91, 0x08, 0x29, 0x17, 0xcc, 0x8f,
        0xd5, 0x3a, 0x22, 0xb3, 0xd1, 0xad, 0x83, 0xdd,
        0xf7, 0xa8, 0xe0, 0x88, 0xb9, 0xdb, 0x61, 0xff,
        0xe1, 0x36, 0x14, 0xf3, 0x84, 0x23, 0xbe, 0xe7,
        0x81, 0x58, 0x97, 0x2b, 0xe2, 0x73, 0x39, 0x79,
        0xbb, 0x04, 0x28, 0x4d, 0x1b, 0xa7, 0xc1, 0xc4,
        0x5d, 0xfc, 0x70, 0x15, 0x09, 0xb0, 0x8a, 0x2d
    );
    $output = array();
    for ($j = 0; $j < 8; $j++) {
        $hash = $pearsonTable[ord($input[0]) + $j];
        for ($i = 0; $i < strlen($input); $i++) {
            //$hash = $pearsonTable[($hash + ord($input[$i])) % sizeof($pearsonTable)];
            $hash = $pearsonTable[$hash ^ ord($input[$i])];
        }
        $output[$j] = $hash;
    }
    return hexToString($output);
}

/* Overview : Verify the hash of a license string
* Args     : license: string, license string
* Return   : Boolean, True if valid, otherwise False
*/
function dsLicenseVerify($license) {
    $matches = [];
    //FULLMATCH
    //LICENSE
    //PRODUCT
    //FEATURE
    //SERIAL?
    //VIN?
    //DATE?
    //SIGNATURE
    if (preg_match("/^LICENSE=([A-F0-9]+) PRODUCT=([A-Z0-9]+) FEATURE=([A-Z0-9]+) (?:SERIAL=([0-9]+) )?(?:VIN=([A-Z0-9]+) )?(?:DATE=([\/0-9]+) )?SIGNATURE=([A-F0-9]+)$/", $license, $matches)) {
        // Hash to verify.
        $hash_to_verify = $matches[7];
        // Match except for hash at end.
        $hash_input = str_replace($hash_to_verify, "", $matches[0]);
        // Compute what the hash should be.
        $hash_output = generateHash($hash_input);
        // Compare the hashes.
        if(strcmp($hash_to_verify, $hash_output) === 0) {
            // The hashes match.
            return True;
        }
    }
    // Hashes did not match or was not valid license.
    return False;
}

// Thanks to http://stackoverflow.com/questions/3831764/php-vin-number-validation-code
function validateVIN($vin) {
    $vin = strtolower($vin);
    if (!preg_match('/^[^\Wioq]{17}$/', $vin)) { 
        return false; 
    }
    
    // Each character in the VIN is converted from a letter to a number, multiplied by a factor, then 
    $weights = array(8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2);

    $transliterations = array(
    "a" => 1, "b" => 2, "c" => 3, "d" => 4,
    "e" => 5, "f" => 6, "g" => 7, "h" => 8,
    "j" => 1, "k" => 2, "l" => 3, "m" => 4,
    "n" => 5, "p" => 7, "r" => 9, "s" => 2,
    "t" => 3, "u" => 4, "v" => 5, "w" => 6,
    "x" => 7, "y" => 8, "z" => 9
    );
    
    $sum = 0;
    
    for($i = 0 ; $i < strlen($vin) ; $i++ ) { // loop through characters of VIN
        // add transliterations * weight of their positions to get the sum
        if(!is_numeric($vin{$i})) {
            $sum += $transliterations[$vin{$i}] * $weights[$i];
        } else {
            $sum += $vin{$i} * $weights[$i];
        }
    }
    
    // Divide by 11
    $checkdigit = $sum % 11;

    if($checkdigit == 10) { // checkdigit of 10 is represented by "X"
        $checkdigit = "x";
    }

    // Check if the result matches the checkdigit.
    return ($checkdigit == $vin{8});
}

function getVINYear($vin) {
    //If position 7 is numeric, position 10 designates a year between 1980 and 
    if(is_numeric($vin{6})) {
        $transliterations = array(
        "A" => 1980, "B" => 1981, "C" => 1982, "D" => 1983,
        "E" => 1984, "F" => 1985, "G" => 1986, "H" => 1987,
        "J" => 1988, "K" => 1989, "L" => 1990, "M" => 1991,
        "N" => 1992, "P" => 1993, "R" => 1994, "S" => 1995,
        "T" => 1996, "V" => 1997, "W" => 1998, "X" => 1999,
        "Y" => 2000, "1" => 2001, "2" => 2002, "3" => 2003,
        "4" => 2004, "5" => 2005, "6" => 2006, "7" => 2007,
        "8" => 2008, "9" => 2009
        );
        return $transliterations[$vin{9}];
    } else {
        $transliterations = array(
        "A" => 2010, "B" => 2011, "C" => 2012, "D" => 2013,
        "E" => 2014, "F" => 2015, "G" => 2016, "H" => 2017,
        "J" => 2018, "K" => 2019, "L" => 2020, "M" => 2021,
        "N" => 2022, "P" => 2023, "R" => 2024, "S" => 2025,
        "T" => 2026, "V" => 2027, "W" => 2028, "X" => 2029,
        "Y" => 2030, "1" => 2031, "2" => 2032, "3" => 2033,
        "4" => 2034, "5" => 2035, "6" => 2036, "7" => 2037,
        "8" => 2038, "9" => 2039
        );
        return $transliterations[$vin{9}];
    }
}

function getVINManufacturer($vin, $continent) {
    // Easily expandable. Some manufacturers only specify 2 digits, but most are assigned 3.
    // The ones that are assigned 2 "fall through" to the second switch statement.
    // The ones that are not identifiable "fall through" again to the "Unknown" case.
    
    // If this needs to be expanded, use this list:
    // https://en.wikibooks.org/wiki/Vehicle_Identification_Numbers_(VIN_codes)/World_Manufacturer_Identifier_(WMI)
    
    // 3-character manufacturer codes.
    switch(substr($vin, 0, 3)) {
    case "AFA": // South Africa
    case "LVS": // Chang An, China
    case "MAJ": // India
    case "MNB": // Thailand
    case "NM0": // Turkey
    case "PE1": // Phillipines
    case "SFA": // UK
    case "VS6": // Spain
    case "WF0": // Germany
    case "WF1": // Germany
        if($continent) {
            return "Europe";
        }
    case "1FA": // US
    case "1FB": // US
    case "1FC": // US
    case "1FD": // US
    case "1FM": // US
    case "1FT": // US
    case "1ZV": // AutoAlliance
    case "2FA": // Canada
    case "2FB": // Canada
    case "2FC": // Canada
    case "2FM": // Canada
    case "2FT": // Canada
    case "3FA": // Mexico
    case "3FE": // Mexico
    case "6FP": // Australia
    case "8AF": // Argentina
    case "9BF": // Brazil
        if($continent) {
            return "North America";
        }
        return "Ford"; // 26 possible codes!
    case "1LN": // USA
    case "3LN": // Mexico
    case "2LM": // Canada
    case "5LM": // USA
    case "2L1": // Limo?
    case "2LJ": // Hearse?
        return "Lincoln";
        
    }
    // 2-character manufacturer codes.
    switch(substr($vin, 0, 2)) {
    case "1L": // USA
    case "5L": 
        return "Lincoln";
    }
    return "Unknown";
}

function getVINModel($vin) {
    $vin = strtoupper($vin);
    if(getVINManufacturer($vin, false) === "Ford") {
        // For Ford vehicles, analyze digits 5-7.
        // If this needs to be expanded, use this list:
        // https://en.wikibooks.org/wiki/Vehicle_Identification_Numbers_(VIN_codes)/Ford/VIN_Codes#American_models
        switch(substr($vin, 4, 3)) {
            // From wiki, unverified.
        case "P01": // SE AWD (older)
        case "P02": // SEL AWD (older)
        case "P06": // S FWD (older)
        case "P07": // SE FWD (older)
        case "P08": // SEL FWD (older)
        case "P0G": // S
        case "P0H": // SE FWD
        case "P0K": // Titanium FWD
        case "P0D": // Titanium AWD
        case "P0U": // S Hybrid
        case "P0L": // SE Hybrid
        case "P0T": // SE AWD
        case "P0P": // SE PHEV
        case "P0R": // Titanium HEV
        case "P0S": // Titanium PHEV
            return "Fusion";
        }
    } else if (getVINManufacturer($vin, false) === "Lincoln") {
        // For Lincoln vehicles, analyze digits 5-7.
        switch(substr($vin, 4, 3)) {
        case "M26": // FWD (older)
        case "M28": // AWD (older)
        case "L2G": // FWD
        case "L2J": // AWD
        case "L2L": // Hybrid
        case "L2M": // FWD Black Label
        case "L2N": // AWD Black Label
        case "L2P": // Hybrid Black Label
            return "MKZ";
        }
    }
    return "Unknown";
}

/* Overview : Verify VIN is supported vehicle
* Args     : vin: string, vehicle id number (VIN)
* Return   : Boolean, True if supported, otherwise False
*/
function dsLicenseVerifyVIN($vin) {
    // Is VIN valid?
    if(validateVIN($vin)) {
        // Is VIN from 2013 or later?
        if(getVINYear($vin) >= 2013) {          
            // Is VIN Ford or Lincoln?
            if(getVINManufacturer($vin, false) === "Ford") {
                // Is VIN Ford Fusion?
                if(getVINModel($vin) === "Fusion") {
                    return true;
                }
            } else if (getVINManufacturer($vin, false) === "Lincoln") {
                // Is VIN Lincoln MKZ?
                if(getVINModel($vin) === "MKZ") {
                    return true;
                }
            }
        }
    }
    // One of the above tests failed.
    return false;
}

/* Overview : Generate generic license string
* Args     : product: string, product name
* Args     : feature: string, feature name
* Args     : date: string, license expiration date (YYYY/MM/DD), empty string if unused
* Args     : serial: string, serial number, empty string if unused
* Args     : vin: string, VIN, empty string if unused
* Return   : license string (string)
*/
function dsLicenseGenerate($product, $feature, $date, $serial, $vin) {
    $output  = "LICENSE=";
    $licensehex = [rand(0, 255), rand(0, 255), rand(0, 255), rand(0, 255)];
    $output .= hexToString($licensehex);
    $output .= " PRODUCT=";
    $output .= $product;
    $output .= " FEATURE=";
    $output .= $feature;
    if ($date !== "") {
        $output .= " DATE=";
        $output .= $date;
    }
    if ($serial !== "") {
        $output .= " SERIAL=";
        $output .= $serial;
    }
    if ($vin !== "") {
        $output .= " VIN=";
        $output .= $vin;
    }
    $output .= " SIGNATURE=";

    $hash = generateHash($output);

    return $output . $hash;
}

/* Overview : Generate license string for Vehicle Power Distribution
* Args     : serial: integer, serial number
* Return   : license string (string)
* Example  : "LICENSE=F012ED26 PRODUCT=PDS FEATURE=BLUETOOTH SERIAL=1234 SIGNATURE=26D2B50CCA7D2C64"
*/
function dsLicenseGenerateVpdBluetooth($serial) {
    if(is_numeric($serial)) {
        if ($serial > 0) {
            return dsLicenseGenerate("PDS", "BLUETOOTH", "", strval($serial), "");
        }
    }
    return "Error: Serial number must be a positive integer";
}

/* Overview : Generate license string for ADAS Kit maintenance
* Args     : vin: string, vehicle id number (VIN)
* Args     : date: string, license expiration date (YYYY/MM/DD)
* Return   : license string (string)
* Example  : "LICENSE=F012ED26 PRODUCT=MKZ FEATURE=MAINTENANCE VIN=3FA6P0D95FR112502 DATE=2017/12/01 SIGNATURE=XXXXXXXXXXXXXXXX"
*/
function dsLicenseGenerateAdasKit($vin, $date) {
    if (dsLicenseVerifyVIN($vin)) {
        if (preg_match("/[0-9][0-9][0-9][0-9]\/[0-9][0-9]\/[0-9][0-9]/", $date)) {
            return dsLicenseGenerate("MKZ", "MAINTENANCE", $date, "", $vin);
        }
        return "Error: Invalid expiration date";
    }
    return "Error: VIN for unsupported vehicle";
}


/* Overview : Example license generation and tests
* Return   : None
*/
function dsLicenseTests() {
    // Valid tests
    $valid = dsLicenseVerify("LICENSE=F012ED26 PRODUCT=PDS FEATURE=BLUETOOTH SERIAL=1234 SIGNATURE=26D2B50CCA7D2C64");
    echo(var_dump($valid));
    $license = dsLicenseGenerateVpdBluetooth(1234);
    echo(var_dump($license));
    $license = dsLicenseGenerateAdasKit("3FA6P0D95FR112502", "2017/12/01"); // 2015 Ford Fusion
    echo(var_dump($license));
    
    // Invalid tests
    $valid = dsLicenseVerify("LICENSE=F012ED26 PRODUCT=PDS FEATURE=BLUETOOTH SERIAL=1234 ");
    echo(var_dump($valid));
    $valid = dsLicenseVerify("LICENSE=F012ED26 PRODUCT=PDS FEATURE=BLUETOOTH SERIAL=1234 SIGNATURE=");
    echo(var_dump($valid));
    $valid = dsLicenseVerify("LICENSE=F012ED26 PRODUCT=PDS FEATURE=BLUETOOTH SERIAL=1234 SIGNATURE=5555555555555555");
    echo(var_dump($valid));
    $license = dsLicenseGenerateVpdBluetooth("");
    echo(var_dump($license));
    $license = dsLicenseGenerateVpdBluetooth(0);
    echo(var_dump($license));
    $license = dsLicenseGenerateVpdBluetooth(-1234);
    echo(var_dump($license));
    $license = dsLicenseGenerateAdasKit("3FA6P0D95FR112502", "");
    echo(var_dump($license));
    $license = dsLicenseGenerateAdasKit("3FA6P0D95FR112502", "abcdefg");
    echo(var_dump($license));
    $license = dsLicenseGenerateAdasKit("3FA6P0D95FR112502", "123456");
    echo(var_dump($license));
    $license = dsLicenseGenerateAdasKit("", "2017/12/01");
    echo(var_dump($license));
    $license = dsLicenseGenerateAdasKit("8ADB39A3836B72354", "2017/12/01");
    echo(var_dump($license));
}

// Actually run the main method.
//dsLicenseTests();
?>