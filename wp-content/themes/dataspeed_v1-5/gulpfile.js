// ## Globals
var browserSync  = require('browser-sync');
var gulp         = require('gulp');
var runSequence  = require('run-sequence');
var postcss      = require('gulp-postcss');
var cssnano      = require('gulp-cssnano');
var atImport     = require("postcss-import");
var rucksack     = require("rucksack-css");

// ### CSS processing pipeline
gulp.task('styles', function () {
  var processors = [
    atImport(),
    require("lost")(),
    require("postcss-cssnext")(),
    require('precss')(),
    rucksack()
  ];

  return gulp.src('css/dev-css/master.css')
    .pipe(postcss(processors))
    .pipe(cssnano({
      discardComments: {
        removeAll: true
      }
    }))
    .pipe(gulp.dest('css/dist'))
    .pipe(browserSync.stream());
});

gulp.task('vendors', function () {
  var processors = [
    atImport(),
    require("lost")(),
    require("postcss-cssnext")(),
    require('precss')(),
    rucksack()
  ];

  return gulp.src('css/vendor/vendor-master.css')
    .pipe(postcss(processors))
    .pipe(cssnano({
      discardComments: {
        removeAll: true
      }
    }))
    .pipe(gulp.dest('css/dist'))
    .pipe(browserSync.stream());
});

// ### Watch
// `gulp watch` - Use BrowserSync to proxy your dev server and synchronize code
// changes across devices. Specify the hostname of your dev server at
// `manifest.config.devUrl`. When a modification is made to an asset, run the
// build step for that asset and inject the changes into the page.
// See: http://www.browsersync.io
gulp.task('watch', function() {
  browserSync.init({
    proxy: 'http://192.168.33.10/dataspeed'
  });
  gulp.watch(['css/dev-css/**/*.css'], ['styles']);
  gulp.watch(['css/vendor/**/*.css'], ['vendors']);
  gulp.watch("*.php").on("change", browserSync.reload);
});



// ### Build
// `gulp build` - Run all the build tasks but don't clean up beforehand.
// Generally you should be running `gulp` instead of `gulp build`.
gulp.task('build', function(callback) {
  runSequence('styles', callback);
});