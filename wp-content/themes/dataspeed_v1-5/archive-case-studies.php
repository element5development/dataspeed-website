<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES FOR CASE STUDIES
*/ ?>

<?php get_header(); ?>

<main class="full-width">

  <!-- PAGE TITLE, FEATURED IMAGE, BREADCRUMBS -->
    <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- POST ARCHIEVE -->
    <?php  
      $year = get_query_var('year');
      $month = get_query_var('monthnum');
    ?>
    <section class="case-studies archieve-filter full-width clearfix">
      <?php echo do_shortcode('[ajax_load_more post_type="case-studies" year=" ' . $year . ' " month=" ' . $month . ' " posts_per_page="2" scroll="false" transition_container="false" button_label="Load More"]'); ?>
    </section>

  <!-- SUPPORTERS LOGO SLIDER -->
    <?php get_template_part( 'template-parts/content', 'logo-slider' ); ?>

</main>

<?php get_footer(); ?>
