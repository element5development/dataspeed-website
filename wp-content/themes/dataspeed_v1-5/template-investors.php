<?php /*
Template Name: Investors
*/ ?>

<?php get_header(); ?>

<main class="full-width">

    <?php get_template_part( 'template-parts/content', 'page-top' ); //PAGE TITLE, FEATURED IMAGE, BREADCRUMBS ?>

    <?php get_template_part( 'template-parts/content', 'landing-contact' ); //CONTACT FORM, INFORMATION ?>

    <?php get_template_part( 'template-parts/content', 'logo-slider' ); //SUPPORTERS LOGO SLIDER ?>

    <?php get_template_part( 'template-parts/content', 'make-preview' ); //PRODUCT PREVIEWS ?> 

    <?php get_template_part( 'template-parts/content', 'insperation' ); //INSPERATION QUOTE ?> 

    <?php get_template_part( 'template-parts/content', 'studies-preview' ); //LATEST CASE STUDIES WITH SPECIAL SCROLLING EFFECT ?>

    <?php get_template_part( 'template-parts/content', 'in-the-news' ); //THREE SPECIFIC POSTS OF FEATURED NEWS ARTICLES ?>

        <?php if ( get_field('background_image') ) { // PAGE CONTENT WITH A BACKGROUND IMAGE
      get_template_part( 'template-parts/content', 'page-content-background' );
    } ?>

</main>

<?php get_footer(); ?>